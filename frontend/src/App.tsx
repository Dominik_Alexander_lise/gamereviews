import React, { useContext } from 'react'
import { BrowserRouter as Router, Switch, Route } from 'react-router-dom'
import { UserData } from 'react-oidc'
import NavBar from './components/NavBar/NavBar'
import Footer from './components/Footer/Footer'
import GamesPage from './components/GamesPage/GamesPage'
import GameDetailsPage from './components/GameDetailsPage/GameDetailsPage'
import CreateGamePage from './components/CreateGamePage/CreateGamePage'
import EditGamePage from './components/EditGamePage/EditGamePage'
import NotFoundPage from './components/NotFoundPage/NotFoundPage'

function App() {
    return (
        <Router>
            <NavBar />
            <main>
                <Switch>
                    <Route path="/" exact component={GamesPage} />
                    <Route
                        path="/games/:id"
                        exact
                        component={GameDetailsPage}
                    />
                    <Route
                        path="/createGame"
                        exact
                        component={CreateGamePage}
                    />
                    <Route
                        path="/editGame/:id"
                        exact
                        component={EditGamePage}
                    />
                    <Route component={NotFoundPage} />
                </Switch>
            </main>
            <Footer />
        </Router>
    )
}

export default App
