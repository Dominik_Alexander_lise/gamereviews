export interface User {
    id?: string
    openId?: string
    firstName?: string
    lastName?: string
    preferredUserName?: string
}
