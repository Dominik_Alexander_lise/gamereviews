export interface CreateOrUpdateGameDto {
    title: string
    publisher: string
    developer: string
    releaseDate: string
    platforms?: string[]
    coverUrl?: string
    trailerUrl?: string
    description?: string
}
