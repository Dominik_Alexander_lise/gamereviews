export interface GameReviewsError {
    message: string
    detailedMessage?: string
}
