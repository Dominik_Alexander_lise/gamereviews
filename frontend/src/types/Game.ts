export interface Game {
    id: string
    title: string
    publisher: string
    developer: string
    releaseDate: string
    platforms: string[] | null
    coverUrl: string
    ratingsAverage?: number
    ratingsCount: number
}
