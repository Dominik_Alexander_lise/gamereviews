import { Rating } from "./Rating";

export interface GameDetails {
  id: string;
  title: string;
  publisher: string;
  developer: string;
  releaseDate: string;
  platforms: string[] | null;
  coverUrl: string;
  trailerUrl: string;
  description: string;
  ratingsAverage?: number;
  ratingsCount: number;
  ratings: Rating[];
}
