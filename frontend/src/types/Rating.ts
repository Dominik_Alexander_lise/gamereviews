import { User } from './User'

export type Rating = {
    id?: string
    author?: User
    pointsAction: number
    pointsAddiction: number
    pointsGraphics: number
    pointsSound: number
    comment?: string
}
