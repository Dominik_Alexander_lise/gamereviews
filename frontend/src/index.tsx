import React from 'react'
import ReactDOM from 'react-dom'
import reportWebVitals from './reportWebVitals'
import './styles/style.scss'
import axios from 'axios'
import AuthenticationWrapper from './AuthenticationWrapper'

// Enable request logging in dev mode
if (process.env.NODE_ENV === 'development') {
    axios.interceptors.request.use((req) => {
        console.log(`${req.method?.toUpperCase()} ${req.url}`)
        return req
    })

    axios.interceptors.response.use((res) => {
        console.log(res.data)
        return res
    })
}

ReactDOM.render(
    <React.StrictMode>
        <AuthenticationWrapper />
    </React.StrictMode>,
    document.getElementById('root')
)

// If you want to start measuring performance in your app, pass a function
// to log results (for example: reportWebVitals(console.log))
// or send to an analytics endpoint. Learn more: https://bit.ly/CRA-vitals
reportWebVitals()
