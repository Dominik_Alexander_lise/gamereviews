export default function toQueryParams(obj: any): string {
    var serializedStrings = []
    for (const [k, v] of Object.entries(obj)) {
        if (
            typeof v === 'string' ||
            typeof v === 'number' ||
            typeof v === 'boolean'
        ) {
            serializedStrings.push(
                encodeURIComponent(k) + '=' + encodeURIComponent(v)
            )
        }
    }
    return serializedStrings.join('&')
}
