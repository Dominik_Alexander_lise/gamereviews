import { useEffect, useReducer, useState } from 'react'
import axios, { AxiosRequestConfig, AxiosResponse } from 'axios'
import { GameReviewsError } from '../types/GameReviewsError'

type State = {
    response?: AxiosResponse
    error?: GameReviewsError
    isLoading: boolean
}

type Action =
    | { type: 'FETCH_INIT' }
    | { type: 'FETCH_SUCCESS'; response: AxiosResponse }
    | { type: 'FETCH_FAILURE'; error: GameReviewsError }

const dataFetchReducer = (state: State, action: Action) => {
    switch (action.type) {
        case 'FETCH_INIT':
            return {
                ...state,
                isLoading: true,
            }
        case 'FETCH_SUCCESS':
            return {
                ...state,
                isLoading: false,
                response: action.response,
            }
        case 'FETCH_FAILURE':
            return {
                ...state,
                isLoading: false,
                error: action.error,
            }
        default:
            throw new Error('Unknown state')
    }
}

export default function useFetch(initialUrl: string, authToken?: string) {
    const [url, setUrl] = useState(initialUrl)
    const [state, dispatch] = useReducer(dataFetchReducer, {
        isLoading: false,
    })
    const [refreshToggle, setRefreshToggle] = useState(false)
    const refresh = () => {
        setRefreshToggle(!refreshToggle)
    }

    useEffect(() => {
        let didCancel = false

        const fetchData = async () => {
            dispatch({ type: 'FETCH_INIT' })
            try {
                const config = requestConfigFromToken(authToken)
                const response = await axios.get(url, config)
                if (!didCancel) {
                    dispatch({ type: 'FETCH_SUCCESS', response })
                }
            } catch (err) {
                const error: GameReviewsError = {
                    message: err.message ?? 'Unknown error.',
                    detailedMessage: `${err.config?.method} ${err.config?.url}`,
                }

                if (!didCancel) {
                    dispatch({ type: 'FETCH_FAILURE', error })
                }
            }
        }

        fetchData()

        return () => {
            didCancel = true
        }
    }, [url, authToken, refreshToggle])

    return { state, setUrl, refresh }
}

export function requestConfigFromToken(
    token?: string
): AxiosRequestConfig | undefined {
    if (token) {
        return {
            headers: {
                Authorization: `Bearer ${token}`,
            },
        }
    }
}
