import { useContext } from 'react'
import { UserData } from 'react-oidc'

enum Permission {
    CREATE_GAME = 'create_game',
    EDIT_GAME = 'edit_game',
    DELETE_GAME = 'delete_game',
}

function useUserHasPermission(permission: Permission) {
    const userData = useContext(UserData)
    return userData.user?.profile?.roles?.includes(permission) === true
}

export { useUserHasPermission as default, Permission }
