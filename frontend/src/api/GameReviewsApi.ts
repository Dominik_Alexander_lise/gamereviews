import { Game } from '../types/Game'
import { GameDetails } from '../types/GameDetails'
import useFetch, { requestConfigFromToken } from './useFetch'
import { GameReviewsError } from '../types/GameReviewsError'
import { GamesFilter } from '../types/GamesFilter'
import toQueryParams from './toQueryParams'
import useAuthToken from './useAuthToken'
import { Rating } from '../types/Rating'
import axios from 'axios'
import { CreateOrUpdateGameDto } from '../types/CreateOrUpdateGameDto'

const API_PREFIX = 'http://localhost:3001/api'
const GET_ALL_GAMES = API_PREFIX + '/games'
const POST_GAME = API_PREFIX + '/games'
const GET_ALL_PLATFORMS = API_PREFIX + '/games/platforms'
const GET_GAME = API_PREFIX + '/games/'
const POST_RATING = (gameId: string) =>
    API_PREFIX + '/games/' + gameId + '/ratings'
const DELETE_GAME = (gameId: string) => API_PREFIX + '/games/' + gameId
const PUT_GAME = (gameId: string) => API_PREFIX + '/games/' + gameId

function getAllGamesUrlFromFilter(filter: GamesFilter): string {
    const params = toQueryParams(filter)
    return GET_ALL_GAMES + '?' + params
}

function useGetAllGames(initialFilter: GamesFilter): {
    games: Game[] | null
    error?: GameReviewsError
    isLoading: boolean
    setFilter(filter: GamesFilter): void
    refresh: () => void
} {
    const authToken = useAuthToken()
    const url = getAllGamesUrlFromFilter(initialFilter)
    const { state, setUrl, refresh } = useFetch(url, authToken)
    const setFilter = (filter: GamesFilter) => {
        const url = getAllGamesUrlFromFilter(filter)
        setUrl(url)
    }
    return {
        games: state?.response?.data,
        error: state.error,
        isLoading: state.isLoading,
        setFilter,
        refresh,
    }
}

function useGetGame(id: string): {
    game?: GameDetails
    error?: GameReviewsError
    isLoading: boolean
    refresh: () => void
} {
    const authToken = useAuthToken()
    const url = GET_GAME + id
    const { state, setUrl, refresh } = useFetch(url, authToken)

    return {
        game: state.response?.data,
        error: state.error,
        isLoading: state.isLoading,
        refresh,
    }
}

function useGetAllPlatforms(): {
    platforms: string[] | null
    error?: GameReviewsError
    isLoading: boolean
} {
    const authToken = useAuthToken()
    const { state } = useFetch(GET_ALL_PLATFORMS, authToken)
    return {
        platforms: state?.response?.data,
        error: state.error,
        isLoading: state.isLoading,
    }
}

function addGameRating(gameId: string, rating: Rating, token: string) {
    const config = requestConfigFromToken(token)
    return axios
        .post(POST_RATING(gameId), rating, config)
        .then((res) => {
            return res.data
        })
        .catch((err) => {
            handleErrorForResponse(err)
        })
}

function createGame(game: CreateOrUpdateGameDto, token: string) {
    const config = requestConfigFromToken(token)
    return axios
        .post(POST_GAME, game, config)
        .then((res) => {
            return res.data
        })
        .catch((err) => {
            handleErrorForResponse(err.response)
        })
}

function updateGame(
    gameId: string,
    game: CreateOrUpdateGameDto,
    token: string
) {
    const config = requestConfigFromToken(token)
    return axios
        .put(PUT_GAME(gameId), game, config)
        .then((res) => {
            return res.data
        })
        .catch((err) => {
            handleErrorForResponse(err.response)
        })
}

function deleteGame(gameId: string, token: string) {
    const config = requestConfigFromToken(token)
    return axios
        .delete(DELETE_GAME(gameId), config)
        .then((res) => res.data)
        .catch((err) => handleErrorForResponse(err.response))
}

function handleErrorForResponse(res: any) {
    throw new Error(`${res.status} - ${res.statusText}`)
}

export {
    useGetAllGames,
    useGetAllPlatforms,
    useGetGame,
    addGameRating,
    createGame,
    updateGame,
    deleteGame,
}
