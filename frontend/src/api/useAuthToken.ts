import { useContext } from 'react'
import { UserData } from 'react-oidc'

export default function useAuthToken() {
    const userData = useContext(UserData)
    return userData.user?.access_token
}
