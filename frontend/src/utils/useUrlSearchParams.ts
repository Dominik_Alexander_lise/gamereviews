import { useEffect, useState } from 'react'
import { useHistory, useLocation } from 'react-router-dom'

function objectFromQueryString(queryString: string) {
    const urlSearchParams = new URLSearchParams(queryString)
    const params: Record<string, string> = {}
    urlSearchParams.forEach((value, key) => {
        params[key] = value
    })
    return params
}

function queryStringFromObject(obj: Record<string, string>, queryString = '') {
    const urlSearchParams = new URLSearchParams(queryString)
    for (const key in obj) {
        const value = obj[key]?.trim()
        if (value && value.length > 0) {
            urlSearchParams.set(key, value)
        } else {
            urlSearchParams.delete(key)
        }
    }
    return urlSearchParams.toString()
}

export default function useUrlSearchParams(): [
    Record<string, string>,
    (params: Record<string, string>) => void
] {
    const location = useLocation()

    const history = useHistory()

    const [urlSearchParams, setUrlSearchParams] = useState(() =>
        objectFromQueryString(location.search)
    )

    useEffect(() => {
        const queryString = queryStringFromObject(
            urlSearchParams,
            location.search
        )
        history.replace({
            search: queryString,
        })
    }, [urlSearchParams, history, location.search])

    return [urlSearchParams, setUrlSearchParams]
}
