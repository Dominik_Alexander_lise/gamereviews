function inputDateFromString(s: string): string | null {
    try {
        return new Date(s).toISOString().split('T')[0]
    } catch (error) {
        return null
    }
}

function stringFromInputDate(inputDate: string): string | null {
    try {
        return new Date(inputDate).toISOString()
    } catch (error) {
        return null
    }
}

export { inputDateFromString, stringFromInputDate }
