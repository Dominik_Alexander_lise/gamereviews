import React from 'react'
import { BrowserRouter as Router, Switch, Route } from 'react-router-dom'
import { Callback, makeAuthenticator, makeUserManager } from 'react-oidc'
import App from './App'

function AuthenticationWrapper() {
    const userManager = makeUserManager({
        authority: 'http://localhost:3003/auth/realms/gamereviews/',
        client_id: 'gamereviews-webapp',
        redirect_uri: `${window.location.origin}/callback`,
        post_logout_redirect_uri: `${window.location.origin}`,
        response_type: 'code',
    })

    const authenticator = makeAuthenticator({
        userManager,
    })

    const AppWithAuth = authenticator(App)

    return (
        <Router>
            <Switch>
                <Route
                    path="/callback"
                    render={(routerProps) => (
                        <Callback
                            onSuccess={() => {
                                routerProps.history.push('/')
                            }}
                            userManager={userManager}
                        />
                    )}
                />
                <AppWithAuth />
            </Switch>
        </Router>
    )
}

export default AuthenticationWrapper
