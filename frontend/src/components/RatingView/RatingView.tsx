import React from 'react'
import { Rating } from '../../types/Rating'
import StarBar from '../StarBar/StarBar'

export default function RatingView({ rating }: { rating: Rating }) {
    return (
        <div className="my-6">
            <h3 className="is-size-4 has-text-centered mb-3">
                {rating.author ? (
                    <strong>{rating.author.preferredUserName}</strong>
                ) : (
                    <em>Deleted User</em>
                )}
            </h3>
            <div className="level my-4">
                <div className="level-item has-text-centered">
                    <div>
                        <p className="heading">Action</p>
                        <StarBar stars={rating.pointsAction} />
                    </div>
                </div>
                <div className="level-item has-text-centered">
                    <div>
                        <p className="heading">Addiction</p>
                        <StarBar stars={rating.pointsAddiction} />
                    </div>
                </div>
                <div className="level-item has-text-centered">
                    <div>
                        <p className="heading">Graphics</p>
                        <StarBar stars={rating.pointsGraphics} />
                    </div>
                </div>
                <div className="level-item has-text-centered">
                    <div>
                        <p className="heading">Sound</p>
                        <StarBar stars={rating.pointsSound} />
                    </div>
                </div>
            </div>
            {rating.comment && (
                <p className="has-text-centered is-italic">{rating.comment}</p>
            )}
        </div>
    )
}
