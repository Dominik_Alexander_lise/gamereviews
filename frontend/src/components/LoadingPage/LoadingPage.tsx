import React from 'react'

export default function LoadingPage() {
    return <h1 className="title has-text-centered has-text-grey">Loading...</h1>
}
