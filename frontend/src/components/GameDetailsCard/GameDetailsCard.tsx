import React, { useState } from 'react'
import { GameDetails } from '../../types/GameDetails'
import YouTubePlayer from '../YouTubePlayer/YouTubePlayer'
import AverageRatingView from '../AverageRatingView/AverageRatingView'
import CopyUrlToClipboardButton from '../CopyUrlToClipboardButton/CopyUrlToClipboardButton'
import RatingView from '../RatingView/RatingView'
import CreateRatingForm, {
    RatingFormResult,
} from '../CreateRatingForm/CreateRatingForm'
import { addGameRating } from '../../api/GameReviewsApi'
import { Rating } from '../../types/Rating'
import useAuthToken from '../../api/useAuthToken'
import LoadingWrapper from '../LoadingWrapper/LoadingWrapper'
import { GameReviewsError } from '../../types/GameReviewsError'

interface GameDetailsCardProps {
    game?: GameDetails
    onCreatedRating?: (rating: Rating) => void
}

export default function GameDetailsCard({
    game,
    onCreatedRating,
}: GameDetailsCardProps) {
    const authToken = useAuthToken()
    const [isCreatingRating, setIsCreatingRating] = useState(false)
    const [createRatingError, setCreateRatingError] =
        useState<GameReviewsError | null>(null)

    const handleCreateRating = (ratingFormResult: RatingFormResult) => {
        const rating: Rating = {
            ...ratingFormResult,
        }

        setIsCreatingRating(true)
        setCreateRatingError(null)

        addGameRating(game!.id, rating, authToken!)
            .then((data) => {
                if (onCreatedRating) {
                    onCreatedRating(data)
                }
            })
            .catch((err) => {
                setCreateRatingError({
                    message: err.message,
                })
            })
            .finally(() => setIsCreatingRating(false))
    }

    return (
        <>
            <div className="columns">
                <div className="column is-5">
                    <figure className="image is-3by4">
                        <img
                            src={game?.coverUrl}
                            alt={`Cover art for ${game?.title}`}
                        />
                    </figure>
                </div>
                <div className="column">
                    <div className="is-flex is-justify-content-space-between">
                        <h1 className="title is-size-1">
                            {game?.title}
                            {game && (
                                <span className="is-size-2 has-text-grey">
                                    {' '}
                                    (
                                    {new Date(
                                        game?.releaseDate
                                    ).getUTCFullYear()}
                                    )
                                </span>
                            )}
                        </h1>
                        <CopyUrlToClipboardButton url={`/games/${game?.id}`} />
                    </div>
                    <p className="subtitle is-size-3 mb-3">
                        <span>{game?.developer}</span> |{' '}
                        <span>{game?.publisher}</span>
                    </p>
                    <AverageRatingView
                        className="mb-5"
                        count={game?.ratingsCount}
                        average={game?.ratingsAverage}
                    />
                    <div className="tags">
                        {game?.platforms?.map((platform) => (
                            <span className="tag is-primary" key={platform}>
                                {platform}
                            </span>
                        ))}
                    </div>
                </div>
            </div>

            <p className="has-line-breaks">{game?.description}</p>
            {game && game?.trailerUrl.length > 0 && (
                <YouTubePlayer url={game.trailerUrl} />
            )}

            <LoadingWrapper
                isLoading={isCreatingRating}
                error={createRatingError}
            >
                <CreateRatingForm onCreateRating={handleCreateRating} />
            </LoadingWrapper>

            {game?.ratings.map((rating) => (
                <RatingView rating={rating} />
            ))}
        </>
    )
}
