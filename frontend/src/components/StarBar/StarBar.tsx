import React from 'react'
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'
import { faStar as faFullStar } from '@fortawesome/free-solid-svg-icons'
import { faStar as faEmptyStar } from '@fortawesome/free-regular-svg-icons'

interface StarBarProps {
    stars: number
    max?: number
    className?: string
}

export default function StarBar({ stars, max = 5, className }: StarBarProps) {
    return (
        <div className={className}>
            {[...Array(stars)].map((e, i) => (
                <FontAwesomeIcon icon={faFullStar} key={i} />
            ))}
            {[...Array(max - stars)].map((e, i) => (
                <FontAwesomeIcon icon={faEmptyStar} key={i} />
            ))}
        </div>
    )
}
