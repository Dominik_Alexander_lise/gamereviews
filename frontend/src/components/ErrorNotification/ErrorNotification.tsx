import React from 'react'

export default function ErrorNotification({
    error,
    onDismiss,
}: {
    error?: string | null
    onDismiss: () => void
}) {
    return (
        <>
            {error && (
                <div className="notification is-danger">
                    <button
                        className="delete"
                        onClick={() => {
                            onDismiss()
                        }}
                    ></button>
                    <strong>An error occured: </strong>
                    {error}
                </div>
            )}
        </>
    )
}
