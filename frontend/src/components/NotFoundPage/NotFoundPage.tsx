import React from 'react'
import { Link } from 'react-router-dom'

export default function NotFoundPage() {
    return (
        <div className="container">
            <section className="section has-text-centered">
                <h1 className="is-size-3">404 - Not Found</h1>
                <Link className="button is-primary mt-4" to="/">
                    Go to Homepage
                </Link>
            </section>
        </div>
    )
}
