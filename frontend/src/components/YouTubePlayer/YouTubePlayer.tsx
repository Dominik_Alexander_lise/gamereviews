import React from 'react'
import styles from './YouTubePlayer.module.scss'

interface YouTubePlayerProps {
    url: string
}

export default function YoutubePlayer({ url }: YouTubePlayerProps) {
    const embedId = embedIdFromYouTubeUrl(url)

    if (!embedId) {
        return null
    }

    return (
        <div className={`${styles.youtubeContainerResponsive} my-5`}>
            <iframe
                src={`http://www.youtube.com/embed/${embedId}`}
                title="YouTube video player"
                frameBorder="0"
                allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture"
                allowFullScreen
            />
        </div>
    )
}

function embedIdFromYouTubeUrl(url: string) {
    var regExp = /.*(?:youtu.be\/|v\/|u\/\w\/|embed\/|watch\?v=)([^#&?]*).*/
    var match = url.match(regExp)
    return match && match[1].length >= 11 ? match[1] : null
}
