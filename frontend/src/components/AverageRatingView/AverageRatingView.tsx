import React, { useEffect, useState } from 'react'
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'
import {
    faStar as faFullStar,
    faStarHalfAlt as faHalfStar,
} from '@fortawesome/free-solid-svg-icons'
import { faStar as faEmptyStar } from '@fortawesome/free-regular-svg-icons'

interface AverageRatingViewProps {
    count?: number
    average?: number
    max?: number
    className?: string
}

export default function AverageRatingView({
    count,
    average,
    max = 5,
    className,
}: AverageRatingViewProps) {
    const [stars, setStars] = useState({
        fullStars: 0,
        halfStars: 0,
        emptyStars: 0,
    })

    useEffect(() => {
        if (average) {
            const roundedAverage = Math.round(average * 2.0) / 2
            const fullStars = Math.floor(roundedAverage)
            const halfStars = Math.round(average - fullStars)
            const emptyStars = max - fullStars - halfStars
            setStars({
                fullStars,
                halfStars,
                emptyStars,
            })
        }
    }, [count, average, max])

    if (!count || !average) {
        return (
            <div className={`has-text-grey ${className}`}>
                No ratings yet...
            </div>
        )
    }

    return (
        <div className={className}>
            {[...Array(stars.fullStars)].map((e, i) => (
                <FontAwesomeIcon icon={faFullStar} key={i} />
            ))}
            {stars.halfStars > 0 && <FontAwesomeIcon icon={faHalfStar} />}
            {[...Array(stars.emptyStars)].map((e, i) => (
                <FontAwesomeIcon icon={faEmptyStar} key={i} />
            ))}
            <span> ({count})</span>
        </div>
    )
}
