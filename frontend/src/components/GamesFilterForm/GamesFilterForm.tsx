import React, { ReactElement } from 'react'
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'
import { faGamepad } from '@fortawesome/free-solid-svg-icons'

import { GamesFilter } from '../../types/GamesFilter'
import { Link } from 'react-router-dom'
import useUserHasPermission, {
    Permission,
} from '../../api/useUserHasPermission'

interface GamesFilterFormProps {
    className?: string
    platforms: string[]
    filter: GamesFilter
    onFilterChange(filter: GamesFilter): void
}

export default function GamesFilterForm({
    className,
    platforms,
    filter,
    onFilterChange,
}: GamesFilterFormProps): ReactElement {
    const userCanCreateGames = useUserHasPermission(Permission.CREATE_GAME)

    function handlePlatformChange(event: React.ChangeEvent<HTMLSelectElement>) {
        onFilterChange({
            ...filter,
            platform: event.target.value,
        })
    }

    function handleSearchTitleChange(
        event: React.ChangeEvent<HTMLInputElement>
    ) {
        onFilterChange({
            ...filter,
            title: event.target.value,
        })
    }

    return (
        <div
            className={`is-flex is-justify-content-space-between is-flex-wrap-wrap ${
                className ?? ''
            }`}
        >
            <div className="field is-grouped">
                <div className="control">
                    <input
                        type="text"
                        className="input"
                        placeholder="Game Title"
                        value={filter.title}
                        onChange={handleSearchTitleChange}
                    />
                </div>
                <div className="control has-icons-left">
                    <span className="select">
                        <select
                            value={filter.platform}
                            onChange={handlePlatformChange}
                        >
                            <option value="">All platforms</option>
                            {platforms?.map((platform) => (
                                <option key={platform}>{platform}</option>
                            ))}
                        </select>
                    </span>
                    <span className="icon is-small is-left">
                        <FontAwesomeIcon icon={faGamepad} />
                    </span>
                </div>
            </div>
            {userCanCreateGames && (
                <div className="field is-grouped is-grouped-right">
                    <Link to="/createGame" className="button is-link">
                        Add Game
                    </Link>
                </div>
            )}
        </div>
    )
}
