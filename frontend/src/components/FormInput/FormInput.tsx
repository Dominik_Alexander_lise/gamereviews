import React from 'react'
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'
import { faExclamationTriangle } from '@fortawesome/free-solid-svg-icons'

interface FormInputProps extends React.InputHTMLAttributes<HTMLInputElement> {
    name: string
    value: string
    onValueChange: (value: string) => void
    error?: string
    disabled?: boolean
}

export default function FormInput({
    name,
    value,
    onValueChange,
    error,
    disabled = false,
    ...props
}: FormInputProps) {
    return (
        <div className="field">
            <label htmlFor={name} className="label">
                {name}
            </label>
            <div className={`control ${error ? 'has-icons-right' : ''}`}>
                <input
                    name={name}
                    type="text"
                    className={`input ${error ? 'is-danger' : ''}`}
                    disabled={disabled}
                    onChange={(e) => {
                        onValueChange(e.target.value)
                    }}
                    value={value}
                    {...props}
                />
                {error && (
                    <>
                        <span className="icon is-small is-right">
                            <FontAwesomeIcon icon={faExclamationTriangle} />
                        </span>
                        <p className="help is-danger">{error}</p>
                    </>
                )}
            </div>
        </div>
    )
}
