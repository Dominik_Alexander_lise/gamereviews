import React, { PropsWithChildren } from 'react'
import { GameReviewsError } from '../../types/GameReviewsError'
import LoadingPage from '../LoadingPage/LoadingPage'
import ErrorPage from '../ErrorPage/ErrorPage'

type LoadingWrapperProps = PropsWithChildren<{
    isLoading: boolean
    error?: GameReviewsError | null
}>

export default function LoadingWrapper({
    isLoading,
    error,
    children,
}: LoadingWrapperProps) {
    if (isLoading) {
        return <LoadingPage />
    }

    if (error) {
        return <ErrorPage error={error} />
    }

    return <>{children}</>
}
