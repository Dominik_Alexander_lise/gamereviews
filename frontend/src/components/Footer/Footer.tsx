import React from 'react'

export default function Footer() {
    return (
        <footer className="footer has-background-white">
            <div className="container">
                <div className="content has-text-centered">
                    <p>
                        <strong>GameReviews</strong> by{' '}
                        <a href="mailto:dominik.alexander@lise.de">
                            Dominik Alexander.
                        </a>
                    </p>
                </div>
            </div>
        </footer>
    )
}
