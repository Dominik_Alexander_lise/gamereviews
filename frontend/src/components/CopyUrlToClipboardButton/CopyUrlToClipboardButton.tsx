import React, { useState } from 'react'
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'
import { faLink } from '@fortawesome/free-solid-svg-icons'
import ReactTooltip from 'react-tooltip'

interface CopyUrlToClipboardButtonProps {
    url: string
    className?: string
}

export default function CopyUrlToClipboardButton({
    url,
    className,
}: CopyUrlToClipboardButtonProps) {
    const [tooltipMessage, setTooltipMessage] = useState('Copy to clipboard')

    function handleClick() {
        let linkToCopy = url
        if (!url.startsWith(window.location.origin)) {
            linkToCopy = window.location.origin + url
        }
        navigator.clipboard.writeText(linkToCopy)
        setTooltipMessage('✓ Copied')
    }

    return (
        <>
            <ReactTooltip
                id="urlToClipboard"
                effect="solid"
                getContent={() => tooltipMessage}
            />

            <button
                className={`button m-3 ${className ? className : ''}`}
                onClick={handleClick}
                data-tip={tooltipMessage}
                data-for="urlToClipboard"
            >
                <span className="icon">
                    <FontAwesomeIcon icon={faLink} />
                </span>
            </button>
        </>
    )
}
