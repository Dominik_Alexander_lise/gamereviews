import React from 'react'
import { render, screen } from '@testing-library/react'
import GameCard from './GameCard'
import { Game } from '../../types/Game'

describe('GameCard', () => {
    test('should display game details (title and publisher)', () => {
        const game: Game = {
            id: '123',
            title: "Game's Title",
            publisher: "Game's Publisher",
            releaseDate: "Game's Release Date",
            coverUrl: 'http://games.cover.url',
            plattforms: ['PC', 'PS5'],
            ratings: [],
        }

        render(<GameCard game={game} />)

        const elementContainingGamesTitle = screen.getByText(
            RegExp(game.title, 'i')
        ) // use regex instead of string to match case-insensitive
        expect(elementContainingGamesTitle).toBeInTheDocument()
        expect(elementContainingGamesTitle).toHaveClass('title')

        const elementContainingGamesPublisher = screen.getByText(game.publisher)
        expect(elementContainingGamesPublisher).toBeInTheDocument()
        expect(elementContainingGamesPublisher).toHaveClass('subtitle')
    })
})
