import React, { useState } from 'react'
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'
import {
    faEdit as editIcon,
    faTrashAlt as deleteIcon,
} from '@fortawesome/free-regular-svg-icons'
import { Game } from '../../types/Game'
import styles from './GameCard.module.scss'
import { Link } from 'react-router-dom'
import { deleteGame } from '../../api/GameReviewsApi'
import useAuthToken from '../../api/useAuthToken'
import ErrorNotification from '../ErrorNotification/ErrorNotification'
import useUserHasPermission, {
    Permission,
} from '../../api/useUserHasPermission'

interface GameCardProps {
    game: Game
    onCardClick: (gameId: string) => void
    onDeleted: (gameId: string) => void
}

export default function GameCard({
    game,
    onCardClick,
    onDeleted,
}: GameCardProps) {
    const [isDeleting, setIsDeleting] = useState(false)
    const [error, setError] = useState<string | null>(null)
    const token = useAuthToken()
    const userCanEditGames = useUserHasPermission(Permission.EDIT_GAME)
    const userCanDeleteGames = useUserHasPermission(Permission.DELETE_GAME)

    function handleDeleteClick() {
        if (window.confirm('Are you sure you wish to delete this item?')) {
            setIsDeleting(true)
            deleteGame(game.id, token!)
                .then(() => {
                    onDeleted(game.id)
                })
                .catch((err) => {
                    setError(err.message)
                })
                .finally(() => {
                    setIsDeleting(false)
                })
        } else {
            console.log('deletion aborted')
        }
    }

    return (
        <div
            className="card is-clickable"
            onClick={() => {
                onCardClick(game.id)
            }}
        >
            <div className={` ${styles['card-spacing-wrapper']}`}>
                <div className="card-image">
                    <figure className="image is-3by4">
                        <img src={game.coverUrl} alt="Game cover" />
                    </figure>
                </div>
                <div className="card-content">
                    <p className="title">{game.title.toUpperCase()}</p>
                    <p className="subtitle">{game.publisher}</p>
                </div>
            </div>

            <ErrorNotification error={error} onDismiss={() => setError(null)} />

            {(userCanEditGames || userCanDeleteGames) && (
                <footer className="card-footer">
                    {userCanEditGames && (
                        <Link
                            to={`/editGame/${game.id}`}
                            className={`button is-info card-footer-item ${styles['card-footer-item-left']}`}
                        >
                            <FontAwesomeIcon icon={editIcon} />
                        </Link>
                    )}
                    {userCanDeleteGames && (
                        <button
                            className={`button is-danger card-footer-item ${
                                styles['card-footer-button']
                            } ${styles['card-footer-item-right']} ${
                                isDeleting ? 'is-loading' : ''
                            }`}
                            onClick={(e) => {
                                e.stopPropagation()
                                handleDeleteClick()
                            }}
                        >
                            <FontAwesomeIcon icon={deleteIcon} />
                        </button>
                    )}
                </footer>
            )}
        </div>
    )
}
