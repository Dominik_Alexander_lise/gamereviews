import React from 'react'
import { useParams } from 'react-router-dom'
import GameDetailsSection from '../GameDetailsSection/GameDetailsSection'

export default function GameDetailsPage() {
    const { id } = useParams<{ id: string }>()

    return (
        <div className="container">
            <section className="section">
                <GameDetailsSection gameId={id} />
            </section>
        </div>
    )
}
