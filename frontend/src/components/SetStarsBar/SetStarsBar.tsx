import React, { useState } from 'react'
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'
import { faStar as faFullStar } from '@fortawesome/free-solid-svg-icons'
import { faStar as faEmptyStar } from '@fortawesome/free-regular-svg-icons'

interface SetStarBarProps {
    value: number
    onChange: (value: number) => void
    max?: number
    className?: string
    hoverColor?: string
}

export default function SetStarsBar({
    max = 5,
    value,
    onChange,
    className,
    hoverColor = '#00d1b2', // Primary color of bulma
}: SetStarBarProps) {
    const [hoverIndex, setHoverIndex] = useState<number | null>(null)

    return (
        <div className={className} onMouseLeave={() => setHoverIndex(null)}>
            {[...Array(max)].map((e, i) => (
                <span
                    key={i}
                    onClick={() => onChange(i + 1)}
                    onMouseEnter={() => setHoverIndex(i)}
                >
                    <FontAwesomeIcon
                        icon={i < value ? faFullStar : faEmptyStar}
                        color={
                            hoverIndex != null && i <= hoverIndex
                                ? hoverColor
                                : undefined
                        }
                    />
                </span>
            ))}
        </div>
    )
}
