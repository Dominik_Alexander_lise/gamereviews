import React from 'react'
import { GameReviewsError } from '../../types/GameReviewsError'

export default function ErrorPage({ error }: { error?: GameReviewsError }) {
    return (
        <div className="notification is-danger">
            <h1 className="is-size-4">
                {error?.message ? error.message : 'Unkown error'}
            </h1>
            {error?.detailedMessage && (
                <p className="is-size-6">{error.detailedMessage}</p>
            )}
        </div>
    )
}
