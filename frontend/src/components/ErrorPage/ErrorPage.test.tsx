import React from 'react'
import { render, screen } from '@testing-library/react'
import ErrorPage from './ErrorPage'
import { GameReviewsError } from '../../types/GameReviewsError'

describe('ErrorPage', () => {
    test('should display error if present', () => {
        // Arrange
        const errorMessage = 'The error message'
        const error: GameReviewsError = { message: errorMessage }

        // Act
        render(<ErrorPage error={error} />)

        // Assert
        expect(screen.getByText(errorMessage)).toBeInTheDocument()
        expect(screen.getByText(errorMessage).parentElement).toHaveClass(
            'is-danger'
        )
    })

    test('should display unknown error if no error is present', () => {
        // Act
        render(<ErrorPage />)

        // Assert
        expect(screen.getByText('Unkown error')).toBeInTheDocument()
    })

    test('should display detailed error message if present', () => {
        // Arrange
        const detailedMessage = 'Detailed error message'
        const error: GameReviewsError = {
            message: 'Error message',
            detailedMessage,
        }

        // Act
        render(<ErrorPage error={error} />)

        // Assert
        expect(screen.getByText(detailedMessage)).toBeInTheDocument()
        expect(screen.getByText(detailedMessage).parentElement).toHaveClass(
            'is-danger'
        )
    })
})
