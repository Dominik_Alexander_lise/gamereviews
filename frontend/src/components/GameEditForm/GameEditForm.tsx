import React, { useReducer, useState } from 'react'
import { useHistory } from 'react-router-dom'
import FormInput from '../FormInput/FormInput'
import FormTextArea from '../FormTextArea/FormTextArea'
import { CreateOrUpdateGameDto } from '../../types/CreateOrUpdateGameDto'
import { createGame } from '../../api/GameReviewsApi'
import useAuthToken from '../../api/useAuthToken'
import SuccessNotification from '../SuccessNotification/SuccessNotification'
import ErrorNotification from '../ErrorNotification/ErrorNotification'
import { GameDetails } from '../../types/GameDetails'
import {
    inputDateFromString,
    stringFromInputDate,
} from '../../utils/dateConversion'

// Reducer State

interface FormInputState {
    value: string
    error?: string
    validator: (value: string) => string | null
}

interface FormState {
    name: FormInputState
    publisher: FormInputState
    developer: FormInputState
    releaseDate: FormInputState
    platforms: FormInputState
    coverUrl: FormInputState
    trailerUrl: FormInputState
    description: FormInputState
}

// Reducer Action

enum FormActionType {
    SET_VALUE,
    SET_ERROR,
    RESET,
}

type FormAction =
    | {
          type: FormActionType.SET_VALUE
          id: keyof FormState
          value: string
      }
    | {
          type: FormActionType.SET_ERROR
          id: keyof FormState
          error: string
      }
    | { type: FormActionType.RESET }

const formReducer = (state: FormState, action: FormAction): FormState => {
    switch (action.type) {
        case FormActionType.SET_VALUE:
            return {
                ...state,
                [action.id]: {
                    value: action.value,
                    validator: state[action.id].validator,
                },
            }
        case FormActionType.SET_ERROR:
            return {
                ...state,
                [action.id]: {
                    ...state[action.id],
                    error: action.error,
                },
            }
        case FormActionType.RESET:
            return initializeReducerState()
        default:
            throw new Error('Unknown state')
    }
}

function initializeReducerState(game?: GameDetails): FormState {
    return {
        name: {
            value: game?.title ?? '',
            validator: (value) => {
                if (value.length === 0) {
                    return 'Name is required'
                }
                return null
            },
        },
        publisher: {
            value: game?.publisher ?? '',
            validator: (value) => {
                if (value.length === 0) {
                    return 'Publisher is required'
                }
                return null
            },
        },
        developer: {
            value: game?.developer ?? '',
            validator: (value) => {
                if (value.length === 0) {
                    return 'Developer is required'
                }
                return null
            },
        },
        releaseDate: {
            value: game?.releaseDate
                ? inputDateFromString(game?.releaseDate) ?? ''
                : '',
            validator: (value) => {
                if (value.length === 0) {
                    return 'Release date is required'
                } else if (isNaN(Date.parse(value))) {
                    // TODO Validate date string
                    return "Release date couldn't get parsed"
                }
                return null
            },
        },
        platforms: {
            value: game?.platforms ? game.platforms.join(', ') : '',
            validator: (value) => {
                return null
            },
        },
        coverUrl: {
            value: game?.coverUrl ?? '',
            validator: (value) => {
                return null
            },
        },
        trailerUrl: {
            value: game?.trailerUrl ?? '',
            validator: (value) => {
                return null
            },
        },
        description: {
            value: game?.description ?? '',
            validator: (value) => {
                return null
            },
        },
    }
}

export default function GameEditForm({
    game,
    onSubmit,
    onCancel,
    isLoading,
}: {
    game?: GameDetails
    onSubmit: (game: CreateOrUpdateGameDto, resetForm: () => void) => void
    onCancel: () => void
    isLoading: boolean
}) {
    const [formState, dispatch] = useReducer(
        formReducer,
        game,
        initializeReducerState
    )

    const handleCreateNewGameClick = () => {
        // Validate the form:
        let validationSuccessful = true

        for (let key in formState) {
            const keyAsKeyOf = key as keyof FormState
            validationSuccessful =
                validationSuccessful && validateInput(keyAsKeyOf)
        }

        // Send POST request if validation succeeded
        if (validationSuccessful) {
            const gameToCreateOrUpdate: CreateOrUpdateGameDto = {
                title: formState.name.value,
                publisher: formState.publisher.value,
                developer: formState.developer.value,
                releaseDate:
                    stringFromInputDate(formState.releaseDate.value) ?? '',
                platforms:
                    formState.platforms.value.length > 0
                        ? formState.platforms.value
                              .split(',')
                              .map((s) => s.trim())
                        : [],
                coverUrl: formState.coverUrl.value,
                trailerUrl: formState.trailerUrl.value,
                description: formState.description.value,
            }

            onSubmit(gameToCreateOrUpdate, () => {
                dispatch({
                    type: FormActionType.RESET,
                })
            })
        }
    }

    const handleCancelClick = () => {
        onCancel()
    }

    function validateInput(key: keyof FormState): boolean {
        const input = formState[key]
        const error = input.validator(input.value)
        if (error) {
            dispatch({
                type: FormActionType.SET_ERROR,
                id: key,
                error,
            })
            return false
        }
        return true
    }

    return (
        <div>
            <FormInput
                name="Name"
                value={formState.name.value}
                onValueChange={(value) => {
                    dispatch({
                        type: FormActionType.SET_VALUE,
                        id: 'name',
                        value,
                    })
                }}
                error={formState.name.error}
                disabled={isLoading}
                onBlur={() => validateInput('name')}
            />
            <FormInput
                name="Publisher"
                value={formState.publisher.value}
                onValueChange={(value) => {
                    dispatch({
                        type: FormActionType.SET_VALUE,
                        id: 'publisher',
                        value,
                    })
                }}
                error={formState.publisher.error}
                disabled={isLoading}
                onBlur={() => validateInput('publisher')}
            />
            <FormInput
                name="Developer"
                value={formState.developer.value}
                onValueChange={(value) => {
                    dispatch({
                        type: FormActionType.SET_VALUE,
                        id: 'developer',
                        value,
                    })
                }}
                error={formState.developer.error}
                disabled={isLoading}
                onBlur={() => validateInput('developer')}
            />
            <FormInput
                name="Release Date"
                value={formState.releaseDate.value}
                onValueChange={(value) => {
                    dispatch({
                        type: FormActionType.SET_VALUE,
                        id: 'releaseDate',
                        value,
                    })
                }}
                error={formState.releaseDate.error}
                disabled={isLoading}
                onBlur={() => validateInput('releaseDate')}
                type="date"
            />
            <FormInput
                name="Platforms (optional, comma separated)"
                value={formState.platforms.value}
                onValueChange={(value) => {
                    dispatch({
                        type: FormActionType.SET_VALUE,
                        id: 'platforms',
                        value,
                    })
                }}
                error={formState.platforms.error}
                disabled={isLoading}
                onBlur={() => validateInput('platforms')}
            />
            <FormInput
                name="Cover URL (optional)"
                value={formState.coverUrl.value}
                onValueChange={(value) => {
                    dispatch({
                        type: FormActionType.SET_VALUE,
                        id: 'coverUrl',
                        value,
                    })
                }}
                error={formState.coverUrl.error}
                disabled={isLoading}
                onBlur={() => validateInput('coverUrl')}
            />
            <FormInput
                name="Trailer URL (optional)"
                value={formState.trailerUrl.value}
                onValueChange={(value) => {
                    dispatch({
                        type: FormActionType.SET_VALUE,
                        id: 'trailerUrl',
                        value,
                    })
                }}
                error={formState.trailerUrl.error}
                disabled={isLoading}
                onBlur={() => validateInput('trailerUrl')}
            />
            <FormTextArea
                name="Description (optional)"
                value={formState.description.value}
                onValueChange={(value) => {
                    dispatch({
                        type: FormActionType.SET_VALUE,
                        id: 'description',
                        value,
                    })
                }}
                error={formState.description.error}
                disabled={isLoading}
                onBlur={() => validateInput('description')}
            />

            <div className="field is-grouped">
                <div className="control">
                    <button
                        className={`button is-link ${
                            isLoading ? 'is-loading' : ''
                        }`}
                        onClick={handleCreateNewGameClick}
                        disabled={isLoading}
                    >
                        {game ? 'Save' : 'Add'}
                    </button>
                </div>
                <div className="control">
                    <button
                        className="button is-link is-light"
                        onClick={handleCancelClick}
                        disabled={isLoading}
                    >
                        Cancel
                    </button>
                </div>
            </div>
        </div>
    )
}
