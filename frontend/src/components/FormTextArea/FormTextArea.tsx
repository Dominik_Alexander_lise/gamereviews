import React from 'react'
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'
import { faExclamationTriangle } from '@fortawesome/free-solid-svg-icons'

interface FormTextAreaProps
    extends React.InputHTMLAttributes<HTMLTextAreaElement> {
    name: string
    value: string
    onValueChange: (value: string) => void
    error?: string
    disabled?: boolean
}

export default function FormTextArea({
    name,
    value,
    onValueChange,
    error,
    disabled = false,
    ...props
}: FormTextAreaProps) {
    return (
        <div className="field">
            <label htmlFor={name} className="label">
                {name}
            </label>
            <div className={`control ${error ? 'has-icons-right' : ''}`}>
                <textarea
                    id={name}
                    className={`textarea ${error ? 'is-danger' : ''}`}
                    disabled={disabled}
                    {...props}
                    onChange={(e) => {
                        onValueChange(e.target.value)
                    }}
                    value={value}
                />
                {error && (
                    <>
                        <span className="icon is-small is-right">
                            <FontAwesomeIcon icon={faExclamationTriangle} />
                        </span>
                        <p className="help is-danger">{error}</p>
                    </>
                )}
            </div>
        </div>
    )
}
