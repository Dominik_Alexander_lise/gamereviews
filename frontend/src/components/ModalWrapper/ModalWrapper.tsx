import React, { PropsWithChildren } from 'react'

type ModalWrapperProps = PropsWithChildren<{
    isActive: boolean
    onModalClose: () => void
}>

export default function ModalWrapper({
    children,
    isActive,
    onModalClose,
}: ModalWrapperProps) {
    return (
        <div className={`modal ${isActive ? 'is-active' : ''}`}>
            <div className="modal-background" onClick={onModalClose}></div>
            <div className="modal-content">
                <div className="box">{children}</div>
            </div>
            <button
                className="modal-close is-large"
                aria-label="close"
                onClick={onModalClose}
            ></button>
        </div>
    )
}
