import React, { useReducer, useState } from 'react'
import { useHistory } from 'react-router-dom'
import FormInput from '../FormInput/FormInput'
import FormTextArea from '../FormTextArea/FormTextArea'
import { CreateOrUpdateGameDto } from '../../types/CreateOrUpdateGameDto'
import { createGame } from '../../api/GameReviewsApi'
import useAuthToken from '../../api/useAuthToken'
import SuccessNotification from '../SuccessNotification/SuccessNotification'
import ErrorNotification from '../ErrorNotification/ErrorNotification'
import GameEditForm from '../GameEditForm/GameEditForm'

export default function CreateGamePage() {
    const history = useHistory()
    const token = useAuthToken()

    const [success, setSuccess] = useState<string | null>(null)
    const [error, setError] = useState<string | null>(null)
    const [isLoading, setIsLoading] = useState(false)

    function handleFormSubmit(
        gameToCreate: CreateOrUpdateGameDto,
        reset: () => void
    ) {
        setIsLoading(true)
        setSuccess(null)
        setError(null)
        createGame(gameToCreate, token!)
            .then((data) => {
                setSuccess('Game successfully created')
                setError(null)
                reset()
            })
            .catch((err) => {
                console.error(err)
                setSuccess(null)
                setError(err.message)
            })
            .finally(() => {
                setIsLoading(false)
            })
    }

    function handleFormCancel() {
        history.goBack()
    }

    return (
        <div className="container">
            <section className="section columns is-centered">
                <div className="column is-half">
                    <h1 className="title">Add Game</h1>
                    <SuccessNotification
                        success={success}
                        onDismiss={() => setSuccess(null)}
                    />
                    <ErrorNotification
                        error={error}
                        onDismiss={() => setError(null)}
                    />
                    <GameEditForm
                        onSubmit={handleFormSubmit}
                        onCancel={handleFormCancel}
                        isLoading={isLoading}
                    />
                </div>
            </section>
        </div>
    )
}
