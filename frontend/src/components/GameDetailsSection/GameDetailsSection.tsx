import React from 'react'
import { useGetGame } from '../../api/GameReviewsApi'
import LoadingWrapper from '../LoadingWrapper/LoadingWrapper'
import GameDetailsCard from '../GameDetailsCard/GameDetailsCard'
import { Rating } from '../../types/Rating'

interface GameDetailsSectionProps {
    gameId: string
}

export default function GameDetailsSection({
    gameId,
}: GameDetailsSectionProps) {
    const { game, error, isLoading, refresh } = useGetGame(gameId)

    const onCreatedRating = (rating: Rating) => {
        refresh()
    }

    return (
        <LoadingWrapper isLoading={isLoading} error={error}>
            <GameDetailsCard game={game} onCreatedRating={onCreatedRating} />
        </LoadingWrapper>
    )
}
