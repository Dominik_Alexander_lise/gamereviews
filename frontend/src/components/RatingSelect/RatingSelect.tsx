import React from 'react'
import SetStarsBar from '../SetStarsBar/SetStarsBar'

interface RatingSelectProps {
    name: string
    value: number
    setValue: (value: number) => void
}

export default function RatingSelect({
    name,
    value,
    setValue,
}: RatingSelectProps) {
    return (
        <div className="control has-text-centered">
            <label className="label">{name}</label>
            <SetStarsBar value={value} onChange={setValue} />
        </div>
    )
}
