import React from 'react'

export default function SuccessNotification({
    success,
    onDismiss,
}: {
    success?: string | null
    onDismiss: () => void
}) {
    return (
        <>
            {success && (
                <div className="notification is-success">
                    <button
                        className="delete"
                        onClick={() => {
                            onDismiss()
                        }}
                    ></button>
                    <strong>Success: </strong>
                    {success}
                </div>
            )}
        </>
    )
}
