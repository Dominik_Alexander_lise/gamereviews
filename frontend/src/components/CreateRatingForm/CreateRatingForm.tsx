import React, { useState } from 'react'
import RatingSelect from '../RatingSelect/RatingSelect'

export interface RatingFormResult {
    pointsAction: number
    pointsAddiction: number
    pointsGraphics: number
    pointsSound: number
    comment?: string
}

interface CreateRatingViewProps {
    onCreateRating: (rating: RatingFormResult) => void
}

export default function CreateRatingForm({
    onCreateRating,
}: CreateRatingViewProps) {
    const [actionRating, setActionRating] = useState(0)
    const [addictionRating, setAddictionRating] = useState(0)
    const [graphicsRating, setGraphicsRating] = useState(0)
    const [soundRating, setSoundRating] = useState(0)
    const [comment, setComment] = useState('')

    const [error, setError] = useState<string | null>()

    const handleOnSubmit = () => {
        setError(null)

        if (actionRating === 0) {
            setError('Please select a rating for action')
            return
        }

        if (addictionRating === 0) {
            setError('Please select a rating for addiction')
            return
        }

        if (graphicsRating === 0) {
            setError('Please select a rating for graphics')
            return
        }

        if (soundRating === 0) {
            setError('Please select a rating for sound')
            return
        }

        const rating: RatingFormResult = {
            pointsAction: actionRating,
            pointsAddiction: addictionRating,
            pointsGraphics: graphicsRating,
            pointsSound: soundRating,
        }

        if (comment.length > 0) {
            rating.comment = comment
        }

        onCreateRating(rating)

        resetForm()
    }

    const handleOnCancel = () => {
        resetForm()
    }

    const resetForm = () => {
        setActionRating(0)
        setAddictionRating(0)
        setGraphicsRating(0)
        setSoundRating(0)
        setComment('')
        setError(null)
    }

    return (
        <div className="p-6">
            {error && (
                <div className="notification is-danger">
                    <button
                        className="delete"
                        onClick={() => {
                            setError(null)
                        }}
                    ></button>
                    {error}
                </div>
            )}
            <div className="field is-grouped is-justify-content-space-between">
                <RatingSelect
                    name="Action"
                    value={actionRating}
                    setValue={setActionRating}
                />
                <RatingSelect
                    name="Addiction"
                    value={addictionRating}
                    setValue={setAddictionRating}
                />
                <RatingSelect
                    name="Graphics"
                    value={graphicsRating}
                    setValue={setGraphicsRating}
                />
                <RatingSelect
                    name="Sound"
                    value={soundRating}
                    setValue={setSoundRating}
                />
            </div>
            <div className="field">
                <label className="label">Comment (optional)</label>
                <div className="control">
                    <textarea
                        rows={1}
                        className="textarea"
                        value={comment}
                        onChange={(e) => {
                            setComment(e.target.value)
                        }}
                    ></textarea>
                </div>
            </div>
            <div className="field is-grouped is-grouped-centered">
                <div className="control">
                    <button className="button is-link" onClick={handleOnSubmit}>
                        Submit
                    </button>
                </div>
                <div className="control">
                    <button
                        className="button is-link is-light"
                        onClick={handleOnCancel}
                    >
                        Cancel
                    </button>
                </div>
            </div>
        </div>
    )
}
