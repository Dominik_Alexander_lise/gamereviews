import React, { useState } from 'react'
import { useHistory, useParams } from 'react-router-dom'
import { useGetGame } from '../../api/GameReviewsApi'
import LoadingWrapper from '../LoadingWrapper/LoadingWrapper'
import GameEditForm from '../GameEditForm/GameEditForm'
import SuccessNotification from '../SuccessNotification/SuccessNotification'
import ErrorNotification from '../ErrorNotification/ErrorNotification'
import useAuthToken from '../../api/useAuthToken'
import { CreateOrUpdateGameDto } from '../../types/CreateOrUpdateGameDto'
import { updateGame } from '../../api/GameReviewsApi'

export default function EditGamePage() {
    const { id } = useParams<{ id: string }>()
    const { game, error, isLoading } = useGetGame(id)

    const history = useHistory()
    const token = useAuthToken()

    const [updateSuccess, setUpdateSuccess] = useState<string | null>(null)
    const [updateError, setUpdateError] = useState<string | null>(null)
    const [isUpdateLoading, setIsUpdateLoading] = useState(false)

    function handleFormSubmit(
        game: CreateOrUpdateGameDto,
        resetForm: () => void
    ) {
        setIsUpdateLoading(true)
        updateGame(id, game, token!)
            .then(() => {
                setUpdateSuccess('Game successfully updated')
            })
            .catch((err) => {
                setUpdateError(err.message)
            })
            .finally(() => {
                setIsUpdateLoading(false)
            })
    }

    function handleFormCancel() {
        history.goBack()
    }

    return (
        <div className="container">
            <section className="section columns is-centered">
                <div className="column is-half">
                    <h1 className="title">Edit Game</h1>
                    <LoadingWrapper isLoading={isLoading} error={error}>
                        <SuccessNotification
                            success={updateSuccess}
                            onDismiss={() => setUpdateSuccess(null)}
                        />
                        <ErrorNotification
                            error={updateError}
                            onDismiss={() => setUpdateError(null)}
                        />
                        <GameEditForm
                            game={game}
                            onSubmit={handleFormSubmit}
                            onCancel={handleFormCancel}
                            isLoading={isUpdateLoading}
                        />
                    </LoadingWrapper>
                </div>
            </section>
        </div>
    )
}
