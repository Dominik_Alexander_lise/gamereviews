import React from 'react'
import * as GameReviewsApi from '../../api/GameReviewsApi'
import GameCard from '../GameCard/GameCard'
import { Game } from '../../types/Game'
import LoadingWrapper from '../LoadingWrapper/LoadingWrapper'
import GamesFilterForm from '../GamesFilterForm/GamesFilterForm'
import { GamesFilter } from '../../types/GamesFilter'
import { useGetAllPlatforms } from '../../api/GameReviewsApi'
import useUrlSearchParams from '../../utils/useUrlSearchParams'
import ModalWrapper from '../ModalWrapper/ModalWrapper'
import GameDetailsSection from '../GameDetailsSection/GameDetailsSection'

export default function GamesPage() {
    const [urlParams, setUrlParams] = useUrlSearchParams()

    const {
        platforms,
        error: errorAllPlatforms,
        isLoading: isLoadingAllPlatforms,
    } = useGetAllPlatforms()

    const {
        games,
        error: errorAllGames,
        isLoading: isLoadingAllGames,
        setFilter: setFilterAllGames,
        refresh,
    } = GameReviewsApi.useGetAllGames(urlParams)

    function handleFilterChange(filter: GamesFilter) {
        setUrlParams({
            ...urlParams,
            ...filter,
        })
        setFilterAllGames(filter)
    }

    function showModal(gameId: string) {
        setUrlParams({
            ...urlParams,
            selectedGame: gameId,
        })
    }

    function dismissModal() {
        setUrlParams({
            ...urlParams,
            selectedGame: '',
        })
    }

    function handleGameDeleted(gameId: string) {
        console.log('game deleted: ' + gameId)
        refresh()
    }

    return (
        <div className="has-background-white-bis">
            <div className="container">
                <section className="section">
                    <LoadingWrapper
                        isLoading={isLoadingAllPlatforms}
                        error={errorAllPlatforms}
                    >
                        <GamesFilterForm
                            platforms={platforms || []}
                            className="mb-5"
                            filter={urlParams}
                            onFilterChange={handleFilterChange}
                        />
                    </LoadingWrapper>
                    <LoadingWrapper
                        isLoading={isLoadingAllGames}
                        error={errorAllGames}
                    >
                        <div className="columns is-multiline">
                            {games?.map((game: Game) => (
                                <div
                                    key={game.id}
                                    className="column is-12-mobile is-4-tablet is-3-desktop"
                                >
                                    <GameCard
                                        game={game}
                                        onCardClick={(game) => showModal(game)}
                                        onDeleted={handleGameDeleted}
                                    />
                                </div>
                            ))}
                        </div>
                    </LoadingWrapper>
                    <ModalWrapper
                        isActive={
                            urlParams.selectedGame != null &&
                            urlParams.selectedGame.length > 0
                        }
                        onModalClose={() => {
                            dismissModal()
                        }}
                    >
                        {urlParams.selectedGame != null &&
                            urlParams.selectedGame.length > 0 && (
                                <GameDetailsSection
                                    gameId={urlParams.selectedGame}
                                />
                            )}
                    </ModalWrapper>
                </section>
            </div>
        </div>
    )
}
