import React, { useContext, useState } from 'react'
import { Link, useHistory } from 'react-router-dom'
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'
import { faSearch } from '@fortawesome/free-solid-svg-icons'
import logo from '../../assets/gamereviews_logo.png'
import { UserData } from 'react-oidc'

function NavBar() {
    const history = useHistory()
    const [isBurgerActive, setBurgerActive] = useState(false)

    function toggleBurgerMenu() {
        setBurgerActive(!isBurgerActive)
    }

    function handleSearchSubmit(e: React.FormEvent) {
        e.preventDefault()
        const target = e.target as typeof e.target & {
            search: { value: string }
        }
        const gameTitleToSearchFor = encodeURIComponent(target.search.value)

        history.push({
            pathname: '/',
            search: `title=${gameTitleToSearchFor}`,
            state: {
                title: gameTitleToSearchFor,
            },
        })
        // TODO There should be a smarter, more elegant way to go to the homepage
        history.go(0)
        target.search.value = ''
    }

    const userData = useContext(UserData)

    return (
        <nav
            className="navbar is-fixed-top has-shadow"
            role="navigation"
            aria-label="main navigation"
        >
            <div className="container">
                <div className="navbar-brand">
                    <Link to="/" className="navbar-item">
                        <img src={logo} alt="Logo" />
                    </Link>
                    <a
                        href="/#"
                        role="button"
                        className={`navbar-burger ${
                            isBurgerActive ? 'is-active' : ''
                        }`}
                        aria-label="menu"
                        aria-expanded="false"
                        data-target="navMenu"
                        onClick={toggleBurgerMenu}
                    >
                        <span aria-hidden="true"></span>
                        <span aria-hidden="true"></span>
                        <span aria-hidden="true"></span>
                    </a>
                </div>
                <div
                    className={`navbar-menu ${
                        isBurgerActive ? 'is-active' : ''
                    }`}
                    id="navMenu"
                >
                    <div className="navbar-end">
                        <div className="navbar-item field">
                            <form onSubmit={handleSearchSubmit}>
                                <p className="control has-icons-right">
                                    <input
                                        type="search"
                                        name="search"
                                        className="input is-rounded"
                                        placeholder="Search..."
                                        onSubmit={handleSearchSubmit}
                                    />
                                    <span className="icon is-small is-right">
                                        <FontAwesomeIcon icon={faSearch} />
                                    </span>
                                </p>
                            </form>
                        </div>
                    </div>
                    <div className="navbar-item">
                        <div className="buttons">
                            <button
                                className="button is-primary"
                                onClick={() => {
                                    userData.userManager?.signoutRedirect()
                                }}
                            >
                                Logout
                            </button>
                        </div>
                    </div>
                </div>
            </div>
        </nav>
    )
}

export default NavBar
