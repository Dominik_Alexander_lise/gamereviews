# Design Patterns

# Creational Patterns

## Singleton Pattern

Genutzt um zentrale Ressource durch definierte Schnittstelle zur Verfügung zu stellen, bspw. eine Datenbank Verbindung.

Umsetzung in der Regel immer gleich: privater Konstruktor, statische (thread-sichere) Methode, um Singleton zu instanziieren.

Meist keine Parameter im Konstruktor nötig. Ansonsten Lazy Initialization Singleton (wie in Android) möglich

In Kotlin ganz einfach per `object Singleton { ... }` realisierbar.

## Builder Pattern

Genutzt, um komplexe Konstruktoren oder Teleskop-Konstruktoren zu vermeiden. Bsp: StringBuilder, DocumentBuilder, Locale.Builder.

In Kotlin fast überflüssig, da benennbare Parameter mit default Werten erlaubt sind:

```kotlin
data class ObjectToBuild(paramOne: String = "", paramTwo: String = "")

val obj = ObjectToBuild(paramTwo = "abc")
```

## Prototype Pattern

- avoid costly creation
- eher ein Refactory Pattern
- avoid subclassing
- typically don't use `new` keyword
- ususally implemented with a registry
- example: java.lang.Object#clone()

Wird genutzt, wenn viele Instanzen einer Klasse benötigt werden und das Erstellen der Objekte teuer ist.

## Factory Pattern

- hidden instantiation logic
- common interface
- examples Calendar, ResourceBundle, NumberFormat

Wird genutzt, wenn es mehrer Implementierungen eines gemeinesamen Interface / einer Abstrakten Klasse gibt und es dem Nutzer dieser Klassen egal sein kann, welche konkrete Klasse genutzt wird.

## Abstract Factory Pattern

- groups factories together
- factory is responsible for lifecycle
- usually uses composition (in contrast to factory pattern)
- examples: DocumentBuilder

# Structural Patterns

## Adapter

Pattern, um Legacy Code in eigenen Code zu wrappen, um ihn an die eigenen Anforderungen zu adaptieren.
Bspw. eine Klasse `EmployeeLdapAdapter(employeeLdap: EmployeeLdap)`, die eine nicht kompatible Klasse `EmployeeLdap` wrappt.

## Proxy

- interface by wrapping
- can add functionality
  - security, simplicity, remote, cost
- Proxy called to access real object
- examples: java.lang.reflect.Proxy, java.rmi.\*

# Behavioral Patterns

## Observer

- decouple object from objects that want to watch it

## Strategy Pattern

- eliminate conditional statements
- behavior encapsulated in classes
- difficult to add new strategies
- client aware of strategies
- client chooses strategy
- examples: java.util.Comparator

- abstract base class
- concrete class per strategy
- remove if/else conditionals
- strategies are independant
- Context, Strategy, ConcreteStrategy

# Additional Notes

Aggregation: object A contains objects B; B can live without A.
Composition: object A consists of objects B; A manages life cycle of B; B can’t live without A.
