# React Learnings

## CORS

Um CORS Fehler zu vermeiden, kann während der Entwicklung ein Proxy genutzt werden, der sämtliche Anfragen auf bspw. `/api` auf einen anderen Host weiterleitet:

```javascript
// setupProxy.js in root of project (the naming matters!)

const { createProxyMiddleware } = require("http-proxy-middleware");

module.exports = (app) => {
  app.use(
    "/api",
    createProxyMiddleware({
      target: "http://localhost:3001",
      changeOrigin: true,
    })
  );
};
```
