# Refactoring

Eine Übersicht über Refactoring von Software.

Refactoring ist oft nicht mehr als aufteilen und bewegen (splitting & moving, more splitting & more moving).

# TOC

- Probleme
  - [Bloaters](#bloaters)
  - Object-Oriented Abusers
  - Change Preventers
  - Couplers
  - Dispensables

# Probleme

## Bloaters

Große Methoden, Klassen und Funktionen, mit denen es schwer fällt, zu arbeiten. Üblicherweise wachsen sie mit der Zeit, wenn sich die Software weiterentwickelt

### Übliche Probleme

- lange Parameterlsiten (>= 4 sind bereits ziemlich viele)
  - ein Magnet für die anderen Probleme Long Methods, Primitive Obsession & Data Clumps
- lange Methoden
  - Lösung: Extract Methods, also aufsplitten mit guter Benennung der Methoden
  - Lösung: Decompose Conditional, also if-Statements in Methode auslagern
- Contrived Complexity (too complex, there is an easier way to do the same thing)
  - Lösung: Algorithmus durch besseren Algorithmus ersetzen
- Primitive Obsession
  - the use of primitives instead of objects
  - Lösung: Preserve Whole Object, also Objekte übergeben statt deren Properties
  - Lösung: Introduce Parameter Object, also eine Klasse erstellen, die die Parameter sinnvoll kapselt
  - Lösung: Enums und Konstanten benutzen
- Data Clumps
  - eine Gruppe Variablen, die zusammen (als Klumpen) innerhalb des Programms herumgereicht wird
  - Lösung: ebenfalls Introduce Parameter Object
- Large Class
  - Klasse mit mehr als einer Verantwortlichkeit
  - Lösung: Klassen aufsplitten

### Grobe Richtlinien

- Methoden und Funktionen
  - bis 10 Zeilen lang
  - oft sind 10-20 Zeilen auch noch okay
  - bei 20+ Zeilen sollte man ein Refactoring definitiv in Erwägung ziehen
- Klassen
  - Nur eine Verantwortlichkeit

## Object-oriented abusers

Code, der keinen OOP Prinzipien folgt.

### Übliche Probleme

- conditional complexity
  - complex switch or if/else statement
  - Lösung: Conditionals mit Polymorphism ersetzen
  - Lösung: Replace with delegation
- refused bequest
  - Unwanted inheritance (bspw. Cat erbt .bark() von Dog)
  - Lösung: Composition statt Vererbung (Composition > Inheritance)
  - Lösung: Methoden generell genug benennen (bspw. .makeSound() für Cat & Dog)
- temporary field
  - properties, die nur unter bestimmten Umständen Werte haben (z.B. nur in einer Methode benutzt werden)
  - Lösung: Extract Class
  - Lösung: Replace Method with Method Object
- alternative classses with different interfaces
  - ggf. Inheritance misuse
  - Lösung: Gemeinsame(s) Interface/Klasse erzeugen
  - Lösung: Nur eine Klasse bleibt
