# Bulma Learnings

## Basics

- Schrifteigenschaften (Größe, Farbe, ...) wird für alle h1-h6 etc resettet

### Schrifteigenschaften

- Größe
    - is-size-[1-7]
- Farben
    - has-text-primary, -warning, -danger, -info, -success, -dark, -light
    - has-background-[primary]
- Modifikatoren
    - is-uppercase, is-lowercase
    - is-italic
    - has-text-weight-bold, has-text-weight-light
    - title, subtitle
    - has-text-centered, has-text-right, has-text-left

### Spacing

- Padding
    - py-[1-6] (padding in y-direction)
    - px-[1-6] (x-direction)
    - p[t]-3: **t**op, **b**ottom, **l**eft, **r**ight
- Margin
    - my-[1-6]
    - mx-[1-6]
    - m[t]-3: **t**op, **b**ottom, **l**eft, **r**ight
- section class="section"
    - automatisch padding auf allen Seiten

### Components

- navbar

### Columns

- div.columns
    - div.column.is-3
    - div.column.is-6
    - div.column.is-3

### Responsiveness

- is-size-1-[desktop] tablet mobile (steht für desktop und größer)
- column is-6-[desktop]
