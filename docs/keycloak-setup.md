# How to setup keycloak

## 1. Compose File anlegen mit etwa diesen angaben:

```YML
version: "3"

volumes:
  postgres_data:
    driver: local

services:
  postgres:
    image: postgres
    volumes:
      - ./postgres_data:/var/lib/postgresql/data
    environment:
      POSTGRES_DB: keycloak
      POSTGRES_USER: keycloak
      POSTGRES_PASSWORD: password
  keycloak:
    image: quay.io/keycloak/keycloak:latest
    environment:
      DB_VENDOR: POSTGRES
      DB_ADDR: postgres
      DB_DATABASE: keycloak
      DB_USER: keycloak
      DB_SCHEMA: public
      DB_PASSWORD: password
      KEYCLOAK_USER: admin
      KEYCLOAK_PASSWORD: Pa55w0rd
      # Uncomment the line below if you want to specify JDBC parameters. The parameter below is just an example, and it shouldn't be used in production without knowledge. It is highly recommended that you read the PostgreSQL JDBC driver documentation in order to use it.
      #JDBC_PARAMS: "ssl=true"
    ports:
      - 3006:8080
    depends_on:
      - postgres

```

## 2. In Admin Console anmelden

http://localhost:3006/auth/admin

```
Username: admin
Password: Pa55w0rd
```

## 3. Realm erzeugen

Links oben auf den Pfeil und `Add Realm` auswählen. 

Entweder:
- Einen Namen wählen (bspw. gamereviews) und alle folgenden Schritte befolgen  

oder
- Import -> Select file und `keycloak/gamereviews-realm-export.json` auswählen und mit Schritt 7 (User anlegen) fortfahren

## 4. Realm Einstellungen

### Login

- User registration: ON
- Forgot password: ON
- Verify email: ON

### Email (Beispiel Gmail)

- Host: smtp.gmail.com
- Port 465

## 5. Client erstellen

Der Client ist die App, die die Authentifizierung in Anspruch nehmen möchte.

Clients -> Create

Name: gamereviews-webapp

### Settings

- Valid Redirect URIs: `http://localhost:3000/*`
- Web Origins `http://localhost3000`

## 6. Roles & Groups konfigurieren

Roles sind in keycloak so etwas wie einzelne Berechtigungen. Groups vereinfachen das Hinzufügen von Roles zu Usern. Ihnen können Roles zugewiesen werden und Usern können Groups zugewiesen werden.

### Roles hinzufügen

Damit das Hinzufügen, Editieren und entfernen von Spielen funktioniert, müssen drei Roles hinzugefügt werden:

Configure:Roles -> Add Role

1. Role Name: create_game
2. Role Name: edit_game
3. Role Name: delete_game

### Groups hinzufügen

Um die Roles einfacher handzuhaben, bietet es sich an zwei Gruppen zu erstellen:

Manage:Groups -> New

1. Name: user
2. Name: author (während user ausgewählt ist, damit author alles kann, was der user kann + zusätzliche Berechtigungen)

Anschließend noch die Roles zur author Group hinzufügen:

author auswählen -> Edit -> Role Mappings -> create_game, edit_game & delete_game auswählen -> Add selected

### Mapper hinzufügen

Damit die eben erstellten Roles auch im id_token auftauchen (und man sie somit im Frontend leichter abfragen kann), muss noch ein Mapper erstellt werden:

Configure:Clients -> gamereviews-webapp -> Edit -> Mappers -> Create

1. Name: roles
2. Mapper Type: User Realm Role
3. Token Claim Name: roles
4. Claim JSON Type: String
5. Add to ID token: ON
6. Add to access token: OFF
7. Add to userinfo: OFF

## 7. User anlegen

Nachdem nun Groups und Roles erstellt sind, können 2 User angelegt werden:

### User anlegen

Manage:Users -> Add user

1. Username: user
2. Email: user@test.de
3. First Name: Hans
4. Last Name: Wurst
5. User Enabled: ON
6. Email Verified: ON
7. Groups: /user

Save -> Credentials -> Password (2x) -> Temporary: OFF -> Set Password

### Author anlegen

Analog für author:

Manage:Users -> Add user

1. Username: author
2. Email: author@test.de
3. First Name: Admin
4. Last Name: Istrator
5. User Enabled: ON
6. Email Verified: ON
7. Groups: /user/author

Save -> Credentials -> Password (2x) -> Temporary: OFF -> Set Password

## 8. OpenID Endpoint Configuration

- issuer: `http://localhost:3003/auth/realms/gamereviews`

(zu finden unter Realm Settings -> General -> Endpoints)

## 9. Frontend konfigurieren (am Beispiel von react-oidc)

```javascript
// AuthenticationWrapper.tsx

function AuthenticationWrapper() {
  const userManager = makeUserManager({
    authority: "http://localhost:3003/auth/realms/gamereviews/",
    client_id: "gamereviews-webapp",
    redirect_uri: `${window.location.origin}/callback`,
    post_logout_redirect_uri: `${window.location.origin}`,
    response_type: "code",
  });

  const authenticator = makeAuthenticator({
    userManager,
  });

  const AppWithAuth = authenticator(App);

  return (
    <Router>
      <Switch>
        <Route
          path="/callback"
          render={(routerProps) => (
            <Callback
              onSuccess={() => {
                routerProps.history.push("/");
              }}
              userManager={userManager}
            />
          )}
        />
        <AppWithAuth />
      </Switch>
    </Router>
  );
}
```

## 10. Backend konfigurieren

```yml
# application.properties

# security
spring.security.oauth2.resourceserver.jwt.issuer-uri=http://localhost:3003/auth/realms/gamereviews
```

```kotlin
// JWTSecurityConfig.kt

package com.example.gamereviews.config

import org.springframework.context.annotation.Configuration
import org.springframework.security.config.annotation.web.builders.HttpSecurity
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter

@Configuration
class JWTSecurityConfig : WebSecurityConfigurerAdapter() {

    override fun configure(http: HttpSecurity?) {
        http?.cors()?.configurationSource {
            val cors = CorsConfiguration()
            cors.allowedOrigins = listOf("http://localhost:3000", "http://127.0.0.1:3000")
            cors.allowedMethods = listOf("GET", "POST", "PUT", "DELETE", "OPTIONS")
            cors.allowedHeaders = listOf("*")
            cors
        } ?.and()
            ?.authorizeRequests { authz -> authz.anyRequest().authenticated() }?.oauth2ResourceServer { oauth2 -> oauth2.jwt() }
    }
}
```
