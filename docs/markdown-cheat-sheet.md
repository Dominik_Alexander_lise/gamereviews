# This is a h1 header
## This is a h2 header
### This is a h3 header

h1 header with underline-ish style
==================================

h2 header with underline-ish style
----------------------------------

*italics* or _italics_
**bold** or __bold__
**combined __bold__**
~~strike through~~

1. Numbered list
2. Example
   1. Sublist
   2. item
   * even possible unordered
* Unordered list with asterisk
- or minus
+ or plus

[This is an inline link](http://google.de)

![This is an image with some alt text](https://raw.githubusercontent.com/adam-p/markdown-here/master/src/common/images/icon48.png)

This is `inline code`

```
This is some code.
```

```javascript
// This is some javascript
const s = "This is a string"
alert(s)
```

| column 1 | column 2 | column 3 |
| -------- | -------- | -------- |
| row 1a   | row 1b   | row 1c   |
| row 2a   | row 2b   | row 2c   |

> This is a blockquote. *Styling* is still possible here

