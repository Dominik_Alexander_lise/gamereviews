package com.example.gamereviews.config

import com.nimbusds.jose.shaded.json.JSONArray
import org.springframework.core.convert.converter.Converter
import org.springframework.security.core.GrantedAuthority
import org.springframework.security.oauth2.jwt.Jwt

class CustomAuthenticationConverter : Converter<Jwt, Collection<GrantedAuthority>> {
    override fun convert(source: Jwt): Collection<LocAuthority>? {
        val permissions = source.getClaimAsMap("realm_access")?.get("roles") as? JSONArray

        return permissions?.toList()?.filterIsInstance<String>()?.mapNotNull { s -> LocAuthority.values().find { a: LocAuthority -> a.stringValue == s } } ?: emptyList()
    }

}
