package com.example.gamereviews.config

import org.springframework.context.annotation.Configuration
import org.springframework.http.HttpMethod
import org.springframework.security.config.Customizer
import org.springframework.security.config.annotation.web.builders.HttpSecurity
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter
import org.springframework.security.oauth2.server.resource.authentication.JwtAuthenticationConverter
import org.springframework.web.cors.CorsConfiguration

@Configuration
class JWTSecurityConfig : WebSecurityConfigurerAdapter() {

    override fun configure(http: HttpSecurity?) {
        http?.cors()?.configurationSource {
            val cors = CorsConfiguration()
            cors.allowedOrigins = listOf("http://localhost:3000", "http://127.0.0.1:3000")
            cors.allowedMethods = listOf("GET", "POST", "PUT", "DELETE", "OPTIONS")
            cors.allowedHeaders = listOf("*")
            cors
        } ?.and()
            ?.authorizeRequests { authz -> authz
                .antMatchers(HttpMethod.POST, "/api/games").hasAuthority(LocAuthority.CREATE_GAME.stringValue)
                .antMatchers(HttpMethod.PUT, "/api/games/**").hasAuthority(LocAuthority.EDIT_GAME.stringValue)
                .antMatchers(HttpMethod.DELETE, "/api/games/**").hasAuthority(LocAuthority.DELETE_GAME.stringValue)
                .anyRequest().authenticated() }?.oauth2ResourceServer { oauth2 -> oauth2.jwt(Customizer { jwt ->
                val jwtAuthenticationConverter = JwtAuthenticationConverter()
                jwtAuthenticationConverter.setJwtGrantedAuthoritiesConverter(CustomAuthenticationConverter())
                jwt.jwtAuthenticationConverter(jwtAuthenticationConverter)
            }) }
    }
}