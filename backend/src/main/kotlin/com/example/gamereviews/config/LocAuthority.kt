package com.example.gamereviews.config

import org.springframework.security.core.GrantedAuthority

enum class LocAuthority(val stringValue: String) : GrantedAuthority {
    CREATE_GAME("create_game"),
    EDIT_GAME("edit_game"),
    DELETE_GAME("delete_game");

    override fun getAuthority(): String {
        return this.stringValue
    }
}
