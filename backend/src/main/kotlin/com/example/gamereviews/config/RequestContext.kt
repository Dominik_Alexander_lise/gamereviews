package com.example.gamereviews.config

import com.example.gamereviews.user.User
import org.springframework.stereotype.Component
import org.springframework.web.context.annotation.RequestScope

@Component
@RequestScope
data class RequestContext(var currentUser: User?)
