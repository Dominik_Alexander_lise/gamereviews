package com.example.gamereviews.config

import com.example.gamereviews.user.UserService
import org.springframework.context.ApplicationListener
import org.springframework.security.authentication.event.AuthenticationSuccessEvent
import org.springframework.security.oauth2.jwt.Jwt
import org.springframework.stereotype.Component

@Component
class AuthenticationListener(private val userService: UserService, private val requestContext: RequestContext): ApplicationListener<AuthenticationSuccessEvent> {
    override fun onApplicationEvent(event: AuthenticationSuccessEvent) {
        val principal = event.authentication.principal
        if (principal is Jwt) {
            val user = userService.getOrCreateUserFromJwt(principal)
            requestContext.currentUser = user
        }
    }
}
