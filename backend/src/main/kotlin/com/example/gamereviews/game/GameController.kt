package com.example.gamereviews.game

import com.example.gamereviews.config.RequestContext
import com.example.gamereviews.game.rating.RatingDto
import org.springframework.http.HttpStatus
import org.springframework.http.ResponseEntity
import org.springframework.web.bind.annotation.*

@RestController
@RequestMapping("/api/games")
class GameController(private val gameService: GameService, private val requestContext: RequestContext) {

    @GetMapping()
    fun getGamesByFilter(
        gamesFilter: GamesFilter?
    ): List<GameDto> {
        val filteredGames = gameService.getGamesByFilter(gamesFilter)
        return mapGamesToDTOs(filteredGames)
    }

    @GetMapping("/{gameId}")
    fun getGameById(@PathVariable("gameId") gameId: String): GameDetailsDto? {
        return gameService.getGameById(gameId)
    }

    @PostMapping
    fun createGame(@RequestBody game: CreateGameDto): GameDto? {
        return gameService.createGame(game)
    }

    @PutMapping("/{gameId}")
    fun updateGame(@PathVariable gameId: String, @RequestBody game: CreateGameDto): GameDto? {
        return gameService.updateGame(gameId, game)
    }

    @DeleteMapping("/{gameId}")
    fun deleteGame(@PathVariable gameId: String): ResponseEntity<Long> {
        return try {
            gameService.deleteGame(gameId)
            ResponseEntity.ok().build()
        } catch (error: Error) {
            println(error)
            ResponseEntity.notFound().build()
        }
    }

    @PostMapping("/{gameId}/ratings")
    fun createRating(@PathVariable gameId: String, @RequestBody rating: RatingDto): RatingDto? {
        validateCreateRating(rating)
        val userId = requestContext.currentUser!!.id
        val ratingEntity = mapRatingDtoToRatingEntity(rating, userId)
        val createdRatingEntity = gameService.createRatingForGame(gameId, ratingEntity)
        return mapRatingEntityToRatingDto(createdRatingEntity)
    }

    @GetMapping("/platforms")
    fun getAllPlatforms(): List<String> {
        return gameService.getAllPlatforms()
    }
}
