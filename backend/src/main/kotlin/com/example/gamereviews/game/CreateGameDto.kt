package com.example.gamereviews.game

import java.time.Instant

data class CreateGameDto(
    val title: String?,
    val publisher: String?,
    val developer: String?,
    val releaseDate: Instant?,
    val platforms: List<String>?,
    val coverUrl: String?,
    val trailerUrl: String?,
    val description: String?,
)