package com.example.gamereviews.game

interface CustomGameRepository {
    fun getAllPlatforms(): List<String>
    fun getGamesByFilter(gamesFilter: GamesFilter?): List<Game>
}
