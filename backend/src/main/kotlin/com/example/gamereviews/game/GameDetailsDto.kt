package com.example.gamereviews.game

import com.example.gamereviews.game.rating.RatingDto
import java.time.Instant

data class GameDetailsDto(
    val id: String?,
    val title: String?,
    val publisher: String?,
    val developer: String?,
    val releaseDate: Instant?,
    val platforms: List<String>?,
    val coverUrl: String?,
    val trailerUrl: String?,
    val description: String?,
    val ratings: List<RatingDto>?,
    var ratingsAverage: Double? = null,
    var ratingsCount: Int? = 0,) {
    constructor(game: Game) : this(
        id = game.id,
        title = game.title,
        publisher = game.publisher,
        developer = game.developer,
        releaseDate = game.releaseDate,
        platforms = game.platforms,
        coverUrl = game.coverUrl,
        trailerUrl = game.trailerUrl,
        description = game.description,
        ratings = game.ratings.map { RatingDto(it) }
    )
}
