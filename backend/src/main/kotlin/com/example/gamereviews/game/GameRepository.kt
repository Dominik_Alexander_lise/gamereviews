package com.example.gamereviews.game

import org.springframework.data.mongodb.repository.MongoRepository
import org.springframework.stereotype.Repository

@Repository
interface GameRepository : CustomGameRepository, MongoRepository<Game, String> {

}
