package com.example.gamereviews.game.rating

import com.example.gamereviews.user.UserDTO

data class RatingDto(
    val id: String?,
    val author: UserDTO?,
    val pointsGraphics: Int?,
    val pointsSound: Int?,
    val pointsAddiction: Int?,
    val pointsAction: Int?,
    val comment: String?,
) {
    constructor(rating: Rating) : this(
        id = rating.id,
        author = UserDTO(rating.author),
        pointsGraphics = rating.pointsGraphics,
        pointsSound = rating.pointsSound,
        pointsAddiction = rating.pointsAddiction,
        pointsAction = rating.pointsAction,
        comment = rating.comment
    )
}
