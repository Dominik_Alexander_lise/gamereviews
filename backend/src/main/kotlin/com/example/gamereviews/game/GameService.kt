package com.example.gamereviews.game

import com.example.gamereviews.game.rating.Rating
import com.example.gamereviews.user.UserService
import org.springframework.data.repository.findByIdOrNull
import org.springframework.stereotype.Service

@Service
class GameService(private val gameRepository: GameRepository, private val userService: UserService) {

    fun getGamesByFilter(gamesFilter: GamesFilter?): List<Game> {
        return gameRepository.getGamesByFilter(gamesFilter)
    }

    fun getAllPlatforms(): List<String> {
        return gameRepository.getAllPlatforms()
    }

    fun getGameById(gameId: String): GameDetailsDto {

        // TODO: I don't think this is the best place to create Dtos
        // maybe I should create three separate classes (GameDto, Game, GameEntity?)

        // 1. get game entity from db
        // 2. get corresponding user entities from db
        // 3. create game dto with user dtos

        val game = gameRepository.findById(gameId)
        if (game.isEmpty) {
            throw Error("Game with ID $gameId not found")
        }
        val gameDto = mapGameToDetailsDTO(game.get())
        gameDto.ratings?.forEach {
            val user = it.author?.id?.let { userId ->
                userService.getUserById(userId) }
            user?.let {  user ->
                it.author.firstName = user.firstName
                it.author.lastName = user.lastName
                it.author.openId = user.userId
                it.author.preferredUserName = user.preferredUserName
            }
        }

        return gameDto
    }

    fun createRatingForGame(gameId: String, rating: Rating): Rating {
        val game = gameRepository.findByIdOrNull(gameId) ?: throw Error("Game with ID $gameId not found")
        // User already rated the game?
        val ratingOfAuthor = game.ratings.find { it.author contentEquals rating.author }
        if (ratingOfAuthor != null) {
            throw Error("Rating from user ${rating.author} already exists for game $gameId")
        }
        game.ratings.add(rating)
        val savedGame = gameRepository.save(game)
        return savedGame.ratings.last()
    }

    fun createGame(game: CreateGameDto): GameDto? {
        val newGame = Game(
            title = game.title!!,
            publisher = game.publisher!!,
            developer = game.developer!!,
            releaseDate = game.releaseDate!!,
            platforms = game.platforms,
            coverUrl = game.coverUrl,
            trailerUrl = game.trailerUrl,
            description = game.description,
            id = "",
            ratings = mutableListOf()
        )
        val savedGame = gameRepository.save(newGame)
        return GameDto(savedGame)
    }

    fun deleteGame(gameId: String) {
        val game = gameRepository.findByIdOrNull(gameId) ?: throw Error("Game with ID $gameId not found")
        gameRepository.delete(game)
    }

    fun updateGame(gameId: String, game: CreateGameDto): GameDto? {
        val gameEntity = gameRepository.findByIdOrNull(gameId) ?: throw Error("Game with ID $gameId not found")
        val newGame = Game(
            title = game.title!!,
            publisher = game.publisher!!,
            developer = game.developer!!,
            releaseDate = game.releaseDate!!,
            platforms = game.platforms,
            coverUrl = game.coverUrl,
            trailerUrl = game.trailerUrl,
            description = game.description,
            id = gameEntity.id,
            ratings = gameEntity.ratings
        )
        val updatedGame = gameRepository.save(newGame)
        return GameDto(updatedGame)
    }
}
