package com.example.gamereviews.game

import java.time.Instant

data class GameDto(
    val id: String?,
    val title: String?,
    val publisher: String?,
    val developer: String?,
    val releaseDate: Instant?,
    val platforms: List<String>?,
    val coverUrl: String?,
    var ratingsAverage: Double? = null,
    var ratingsCount: Int? = 0,
) {
    constructor(game: Game) : this(
        id = game.id,
        title = game.title,
        publisher = game.publisher,
        developer = game.developer,
        releaseDate = game.releaseDate,
        platforms = game.platforms,
        coverUrl = game.coverUrl,
    )
}