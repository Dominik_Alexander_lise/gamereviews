package com.example.gamereviews.game

import org.springframework.data.mongodb.core.MongoTemplate
import org.springframework.data.mongodb.core.query.*
import org.springframework.data.mongodb.core.query.Query

class CustomGameRepositoryImpl(private val mongoTemplate: MongoTemplate) : CustomGameRepository {

    override fun getAllPlatforms(): List<String> {
        return mongoTemplate.findDistinct("platforms", Game::class.java, String::class.java)
    }

    override fun getGamesByFilter(gamesFilter: GamesFilter?): List<Game> {
        val query = Query()

        if (gamesFilter != null) {
            if (!gamesFilter.title.isNullOrBlank()) {
                query.addCriteria(Game::title regex Regex(gamesFilter.title, RegexOption.IGNORE_CASE))
            }
            if (!gamesFilter.platform.isNullOrBlank()) {
                query.addCriteria(Game::platforms isEqualTo gamesFilter.platform)
            }
        }
        return mongoTemplate.find(query, Game::class.java)
    }
}
