package com.example.gamereviews.game

import com.example.gamereviews.game.rating.Rating
import com.example.gamereviews.game.rating.RatingDto
import org.bson.types.ObjectId

fun calculateAverageRating(rating: Rating): Double {
    return (rating.pointsAction + rating.pointsAddiction + rating.pointsGraphics + rating.pointsSound) / 4.0
}

fun calculateAverageGameRating(ratings: List<Rating>): Double {
    return ratings.fold(0.0) { acc, rating -> acc + calculateAverageRating(rating) } / ratings.size
}

fun mapGameToDTO(game: Game): GameDto {
    val gameDTO = GameDto(game)

    val ratingsCount = game.ratings.size
    if (ratingsCount > 0) {
        gameDTO.ratingsAverage = calculateAverageGameRating(game.ratings)
    }

    return gameDTO
}

fun mapGamesToDTOs(games: List<Game>): List<GameDto> {
    return games.map { mapGameToDTO(it) }
}

fun mapGameToDetailsDTO(game: Game): GameDetailsDto {
    val gameDetailsDTO = GameDetailsDto(game)

    val ratingsCount = game.ratings.size
    gameDetailsDTO.ratingsCount = ratingsCount
    if (ratingsCount > 0) {
        gameDetailsDTO.ratingsAverage = calculateAverageGameRating(game.ratings)
    }

    return gameDetailsDTO
}

fun mapRatingDtoToRatingEntity(rating: RatingDto, userId: String): Rating {

    // TODO: is this the right place to create the new ObjectId?
    val newRatingId = ObjectId().toString()

    return Rating(newRatingId,
        userId,
        rating.pointsAction!!,
        rating.pointsAddiction!!,
        rating.pointsGraphics!!,
        rating.pointsSound!!,
        rating.comment
    )
}

fun mapRatingEntityToRatingDto(rating: Rating): RatingDto {
    return RatingDto(rating)
}