package com.example.gamereviews.game.rating

import org.springframework.data.annotation.Id

data class Rating(
    @Id val id: String,
    val author: String,
    val pointsAction: Int,
    val pointsAddiction: Int,
    val pointsGraphics: Int,
    val pointsSound: Int,
    val comment: String?,
    )
