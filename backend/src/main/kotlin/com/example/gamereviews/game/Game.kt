package com.example.gamereviews.game

import org.springframework.data.annotation.Id
import org.springframework.data.mongodb.core.mapping.Document
import java.time.Instant
import com.example.gamereviews.game.rating.Rating

@Document("games")
data class Game(
    @Id val id: String,
    val title: String,
    val publisher: String,
    val developer: String,
    val releaseDate: Instant,
    val platforms: List<String>?,
    val coverUrl: String?,
    val trailerUrl: String?,
    val description: String?,
    val ratings: MutableList<Rating>
    )
