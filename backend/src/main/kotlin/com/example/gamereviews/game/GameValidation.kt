package com.example.gamereviews.game

import com.example.gamereviews.game.rating.RatingDto
import org.springframework.http.HttpStatus
import org.springframework.web.server.ResponseStatusException

fun validateRatingPoints(points: Int?, name: String) {
    if (points == null || points < 1 || points > 5) {
        throw ResponseStatusException(HttpStatus.BAD_REQUEST, "$name is required and has to be in range of 1 to 5")
    }
}

fun validateCreateRating(rating: RatingDto) {
    validateRatingPoints(rating.pointsAction, "pointsAction")
    validateRatingPoints(rating.pointsAddiction, "pointsAddiction")
    validateRatingPoints(rating.pointsGraphics, "pointsGraphics")
    validateRatingPoints(rating.pointsSound, "pointsSound")
}