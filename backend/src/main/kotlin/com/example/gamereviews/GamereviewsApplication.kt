package com.example.gamereviews

import org.springframework.boot.autoconfigure.SpringBootApplication
import org.springframework.boot.autoconfigure.security.servlet.SecurityAutoConfiguration
import org.springframework.boot.runApplication

// If there are problems with Authorization you can exclude SecurityAutoConfiguration like this:
// @SpringBootApplication(exclude = [SecurityAutoConfiguration::class])
@SpringBootApplication
class GamereviewsApplication

fun main(args: Array<String>) {
	runApplication<GamereviewsApplication>(*args)
}
