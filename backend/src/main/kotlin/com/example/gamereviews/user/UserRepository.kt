package com.example.gamereviews.user

import org.springframework.data.mongodb.repository.MongoRepository
import org.springframework.stereotype.Repository

@Repository
interface UserRepository : CustomUserRepository, MongoRepository<User, String> {
    fun findByUserId(userId: String): User?
}
