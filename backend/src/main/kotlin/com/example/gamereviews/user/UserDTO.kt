package com.example.gamereviews.user

data class UserDTO(
    val id: String? = null,
    var openId: String? = null,
    var firstName: String? = null,
    var lastName: String? = null,
    var preferredUserName: String? = null
    ) {
    constructor(user: User) : this(
        id = user.id,
        openId = user.userId,
        firstName = user.firstName,
        lastName = user.lastName,
        preferredUserName = user.preferredUserName
    )
}
