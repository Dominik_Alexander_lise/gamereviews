package com.example.gamereviews.user

import org.springframework.data.mongodb.core.MongoTemplate
import org.springframework.data.mongodb.core.query.Query
import org.springframework.data.mongodb.core.query.Update
import org.springframework.data.mongodb.core.query.where

interface CustomUserRepository {
    fun upsertUser(
        userId: String,
        firstName: String,
        lastName: String,
        preferredName: String,
        email: String
    )
}

class CustomUserRepositoryImpl(private val mongoTemplate: MongoTemplate) : CustomUserRepository {
    override fun upsertUser(
        userId: String,
        firstName: String,
        lastName: String,
        preferredUserName: String,
        email: String
    ) {
        val updateResult = mongoTemplate.upsert(
            Query( where(User::userId ).`is`(userId) ),
            Update().set(
                User.FirstNameProperty,
                firstName
            ).set(
                User.LastNameProperty,
                lastName
            ).set(
                User.EmailProperty,
                email
            ).set(
                User.PreferredUserNameProperty,
                preferredUserName
            ).setOnInsert(
                User.UserIdProperty,
                userId
            ),
            User::class.java
        )
    }
}
