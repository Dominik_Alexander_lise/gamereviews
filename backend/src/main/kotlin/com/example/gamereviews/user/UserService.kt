package com.example.gamereviews.user

import org.springframework.security.oauth2.core.oidc.StandardClaimNames
import org.springframework.security.oauth2.jwt.Jwt
import org.springframework.stereotype.Service

@Service
class UserService(private val userRepository: UserRepository) {
    fun getOrCreateUserFromJwt(jwt: Jwt): User {
        val userId = jwt.getClaimAsString(StandardClaimNames.SUB)
        val firstName = jwt.getClaimAsString(StandardClaimNames.GIVEN_NAME)
        val lastName = jwt.getClaimAsString(StandardClaimNames.FAMILY_NAME)
        val preferredUserName = jwt.getClaimAsString(StandardClaimNames.PREFERRED_USERNAME)
        val email = jwt.getClaimAsString(StandardClaimNames.EMAIL)

        userRepository.upsertUser(userId, firstName, lastName, preferredUserName, email)

        return userRepository.findByUserId(userId)!!
    }

    fun getUserById(userId: String): User? {
        val user = userRepository.findById(userId)
        if (user.isEmpty) {
            return null
        }
        return user.get()
    }
}