package com.example.gamereviews.user

import org.springframework.data.annotation.Id
import org.springframework.data.mongodb.core.mapping.Document
import org.springframework.data.mongodb.core.mapping.Field

@Document(User.CollectionName)
data class User(
    @Id val id: String,
    @Field(UserIdProperty) var userId: String,
    @Field(FirstNameProperty) var firstName: String?,
    @Field(LastNameProperty) var lastName: String?,
    @Field(EmailProperty) var email: String?,
    @Field(PreferredUserNameProperty) var preferredUserName: String?
    ) {
    companion object {
        const val CollectionName = "users"
        const val UserIdProperty = "userId"
        const val FirstNameProperty = "firstName"
        const val LastNameProperty = "lastName"
        const val EmailProperty = "email"
        const val PreferredUserNameProperty = "preferredUserName"
    }
}
