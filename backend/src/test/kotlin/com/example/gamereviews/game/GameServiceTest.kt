package com.example.gamereviews.game

import io.mockk.every
import io.mockk.mockk
import org.assertj.core.api.Assertions.*
import org.junit.jupiter.api.*
import java.time.Instant

internal class GameServiceTest {

    private val gameRepository = mockk<GameRepository>()
    private val gameOne = Game("1", "FIFA 21", "EA Sports", "EA Vancouver", Instant.now(), null, null, emptyList())
    private val gameTwo = Game("2", "Dishonored", "Bethesda", "Arkane Studios", Instant.now(), null, null, emptyList())
    private val gameThree =
        Game("3", "Hades", "Private Division", "Private Division", Instant.now(), null, null, emptyList())
    private val gameService = GameService(gameRepository)

    @Test
    fun `should get all games from repository`() {
        // Arrange
        every { gameRepository.findAll() } returns listOf(gameOne, gameTwo, gameThree)

        // Act
        val result = gameService.getAllGames()

        // Assert
        assertThat(3).isEqualTo(result.size);
        assertThat(gameOne.id).isEqualTo(result.first().id);
        assertThat(gameOne).isEqualToComparingFieldByField(result.first());
    }
}
