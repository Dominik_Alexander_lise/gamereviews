package com.example.gamereviews.game

import com.example.gamereviews.game.rating.Rating
import com.example.gamereviews.game.rating.RatingDto
import org.assertj.core.api.Assertions

import org.junit.jupiter.api.Test

import org.junit.jupiter.api.Assertions.*
import org.junit.jupiter.api.DisplayName
import java.time.Instant

@DisplayName("ObjectMapperUtils should")
internal class ObjectMapperUtilsTest {

    @Test
    fun `map Game object to GameDTO object`() {
        val ratings = listOf(Rating("Rating ID", "Author ID", 3, "Rating Comment"))
        val game = Game("Game ID", "Game Title", "Game Publisher", "Game Developer", Instant.now(), listOf("Game Platform 1", "Game Platform 2"), "http://game.cover.url", ratings)
        val gameDTO = mapGameToDTO(game)
        assertGameEquals(game, gameDTO)
        Assertions.assertThat(game).isEqualToIgnoringGivenFields(gameDTO, "ratings")
        Assertions.assertThat(game.ratings.first()).isEqualToComparingFieldByField(gameDTO.ratings?.first())
    }

    @Test
    fun `map Game objects to GameDTO objects`() {
        val gameOne = Game("Game ID", "Game Title", "Game Publisher", "Game Developer", Instant.now(), null, null, emptyList())
        val gameTwo = Game("GameTwo ID", "GameTwo  Title", "GameTwo Publisher", "GameTwo Developer", Instant.now(), null, null, emptyList())
        val games = listOf(gameOne, gameTwo)
        val gameDTOs = mapGamesToDTOs(games)
        assertEquals(games.size, gameDTOs.size)
        assertGamesEquals(games, gameDTOs)
    }

    private fun assertGamesEquals(games: List<Game>, gameDtos: List<GameDto>) {
        games.forEachIndexed{ index, game ->
            assertGameEquals(game, gameDtos[index])
        }
    }

    private fun assertGameEquals(game: Game, gameDTO: GameDto) {
        assertEquals(game.id, gameDTO.id)
        assertEquals(game.title, gameDTO.title)
        assertEquals(game.publisher, gameDTO.publisher)
        assertEquals(game.developer, gameDTO.developer)
        assertEquals(game.releaseDate, gameDTO.releaseDate)
        assertEquals(game.platforms, gameDTO.platforms)
        assertEquals(game.coverUrl, gameDTO.coverUrl)
        assertRatingsEquals(game.ratings, gameDTO.ratings)
    }

    private fun assertRatingsEquals(ratings: List<Rating>, ratingDtos: List<RatingDto>?) {
        ratings.forEachIndexed { index, rating ->
            assertRatingEquals(rating, ratingDtos!![index])
        }
    }

    private fun assertRatingEquals(rating: Rating, ratingDTO: RatingDto) {
        assertEquals(rating.id, ratingDTO.id)
        assertEquals(rating.author, ratingDTO.author)
        assertEquals(rating.points, ratingDTO.points)
        assertEquals(rating.comment, ratingDTO.comment)
    }
}