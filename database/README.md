# Datenbankkonfiguration für das Übungsprojekt *gamereviews*

## mongoDB Container erzeugen und starten

Um den Docker Container, der den nötige mongoDB-Server zur Verfügung stellt zu starten, muss [Docker](https://www.docker.com/get-started) (inklusive CLI) installiert sein.

Mit folgendem Befehl kann der Container gestartet werden
```
docker run --name gamereviews-mongo -p 27017:27017 -v C:\Users\Pipo\Documents\Programming\lise\traineeproject\gamereviews\database\data\db:/data/db -e MONGO_INITDB_ROOT_USERNAME=mongoadmin -e MONGO_INITDB_ROOT_PASSWORD=secret -d mongo:latest
```

`--name gamereviews-mongo` spezifiziert den Namen des Containers

`-p 27017:27017` ist ein Portmapping des Hosts auf den Container

`-v C:\absolute\path:/path/in/container` stellt ein Volume für den Austausch von Daten zwischen Host und Container zur Verfügung

`-d` steht für derived, also im Hintergrund auszuführen

`-e` setzt die Umgebungsvariablen (**e**nvironment) für die Zugangsdaten für mongoDB

`mongo:latest` holt sich vom Docker Hub die aktuellste Version dieses Containers und führt sie aus (alternativ `mongo:4`, dann updated sich der Container nicht automatisch auf v5, wenn diese auf latest gesetzt wird)

## Alternative: mongoDB Container mit docker-compose starten

Alternativ kann auch docker-compose zum Starten der Datenbank genutzt werden: (dazu muss Docker Desktop laufen)
```
docker-compose -f docker-compose.yml up
```

docker-compose.yml
```yml
version: "3.7"

services:
    gamereviews-mongodb:
        image: mongo:4.4.6
        ports:
            - 27017:27017
        volumes:
            - ./data/db:/data/db
        environment:
            MONGO_INITDB_ROOT_USERNAME: root
            MONGO_INITDB_ROOT_PASSWORD: example
```


# Mit dem mongoDB-Container arbeiten

## Mit mongoDB Container verbinden und dort bash ausführen
```
docker exec -it gamereviews-mongo bash
```

## Um dann dort die mongo Shell auszuführen
```
mongo --username root --password example
```

## Ein paar Befehle für die mongo Shell:

### Hilfe anzeigen
```
help
```

### Datenbanken auflisten
```
show dbs
```

### Collections in der aktuellen Datenbank auflisten
```
show collections
```

### Aktuelle Datenbank auswählen
```
use <db_name>
```

### Hilfe für db Methoden
```
db.help()
```

### Hilfe für collection Methoden
```
db.mycoll.help()
db.game.help()
```

### Hilfe für DBCursor
```
db.game.find().help()
```

### Alle auf der db befindlichen collections abfragen
```
db.getCollectionNames()
db.getCollectionInfos([filter])
```

### Eine konkrete Collection abfragen (alle äquivalent)
```
db.getCollection('game')
db['game']
db.game
```

### Anzahl Einträge abfragen
```
db.game.count()
```

### CRUD Operations
```
db.game.insert(obj)
db.game.insertOne(obj)
db.game.insertMany([objects])

db.game.find([query], [fields])
db.game.find({title:"FIFA"}, {id:1, title:1})

db.game.findOne([query], [fields])
db.game.findOneAndUpdate(filter)
db.game.findOneAndDelete(filter)

db.game.update(query)
db.game.updateOne(filter)
db.game.updateMany(filter)

db.game.deleteOne(filter)
db.game.deleteMany(filter)
```

### Komplexere Abfragen

#### Per RegEx Nach *disho* case-insensitive suchen (findet ensprechend beide Dishonored Teile)
```
db.game.find(title:/disho/i)
```

