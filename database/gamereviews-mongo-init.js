// Setup validation schema for games collection
db.createCollection("games", {
  validator: {
    $jsonSchema: {
      bsonType: "object",
      title: "a game",
      description:
        "represents a game with it's title, publisher, developer, etc.",
      required: ["title", "publisher", "developer", "releaseDate", "ratings"],
      additionalProperties: true,
      properties: {
        _id: {
          bsonType: "objectId",
        },
        title: {
          bsonType: "string",
          title: "game title",
          description:
            "a games title. must be a string, at least 1 character and is required.",
          minLength: 1,
        },
        publisher: {
          bsonType: "string",
          title: "publisher of the game",
          description:
            "a games publisher. must be a string, at least 1 character and is required.",
          minLength: 1,
        },
        developer: {
          bsonType: "string",
          title: "developer of the game",
          description:
            "a games developer. must be a string, at least 1 character and is required.",
          minLength: 1,
        },
        releaseDate: {
          bsonType: "date",
          title: "release date of the game",
          description: "a games release date. must be a date and is required.",
        },
        platforms: {
          bsonType: "array",
          title: "platforms of the game",
          description:
            "platforms on which the game is released. array of strings. optional.",
          items: {
            bsonType: "string",
            minLength: 1,
          },
        },
        coverUrl: {
          bsonType: "string",
          title: "URL of the cover of a game",
          description: "URL of the cover of a game. optional.",
        },
        trailerUrl: {
          bsonType: "string",
          title: "URL of the trailer of a game",
          description: "URL of the trailer of a game. optional.",
        },
        description: {
          bsonType: "string",
          title: "Summary of a game",
          description: "Summary of a game. optional.",
        },
        ratings: {
          bsonType: "array",
          title: "ratings of the game",
          description: "must be an array and is required",
          items: {
            bsonType: "object",
            title: "a rating of a game",
            description: "a rating of a game created by an user.",
            required: [
              "_id",
              "author",
              "pointsAction",
              "pointsAddiction",
              "pointsGraphics",
              "pointsSound",
            ],
            additionalProperties: true,
            properties: {
              _id: {
                bsonType: "objectId",
                title: "id of the rating",
                description: "id of the rating. an objectid. is required.",
              },
              author: {
                bsonType: "string",
                title: "id of the author",
                description:
                  "_id of the author as a foreign key from keycloak. is required.",
              },
              pointsAction: {
                bsonType: "int",
                title: "the action rating",
                description:
                  "the action rating in stars. is required, 1 <= points <= 5.",
                minimum: 1,
                maximum: 5,
              },
              pointsAddiction: {
                bsonType: "int",
                title: "the addiction rating",
                description:
                  "the addiction rating in stars. is required, 1 <= points <= 5.",
                minimum: 1,
                maximum: 5,
              },
              pointsGraphics: {
                bsonType: "int",
                title: "the graphics rating",
                description:
                  "the graphics rating in stars. is required, 1 <= points <= 5.",
                minimum: 1,
                maximum: 5,
              },
              pointsSound: {
                bsonType: "int",
                title: "the sound rating",
                description:
                  "the sound rating in stars. is required, 1 <= points <= 5.",
                minimum: 1,
                maximum: 5,
              },
              comment: {
                bsonType: "string",
                title: "review comment for a game",
                description:
                  "review comment for a game. is optional, length >= 1.",
              },
            },
          },
        },
      },
    },
  },
});

// Setup validation schema for users collection
db.createCollection("users", {
  validator: {
    $jsonSchema: {
      bsonType: "object",
      title: "a user",
      description: "represents a user to store additional data",
      required: ["userId"],
      additionalProperties: true,
      properties: {
        _id: {
          bsonType: "objectId",
        },
        userId: {
          bsonType: "string",
          title: "open id sub",
          description: "the open id of the user. required.",
          minLength: 1,
        },
      },
    },
  },
});

// Configure users collection to use unique index to prevent duplicate entries (important for race conditions)
db.users.createIndex({ userId: 1 }, { unique: true });

// Setup initial data in games collection
db.games.insert([
  {
    title: "Dishonored",
    publisher: "Bethesda Softworks",
    developer: "Arkane Studios",
    releaseDate: new Date("2012-10-08"),
    platforms: ["PC", "PlayStation 3", "Xbox 360"],
    coverUrl:
      "https://images.igdb.com/igdb/image/upload/t_cover_big/co1y2j.jpg",
    trailerUrl: "https://www.youtube.com/watch?v=u4b_pKoTebk",
    description:
      "Approach each assassination with your own unique style. Use shadow and sound to your advantage to traverse silently through levels unseen by enemies, or attack foes head-on as they react to your aggression. The malleable combat system allows you to creatively synthesize your abilities, supernatural powers and gadgets as you negotiate your way through the levels and dispatch your targets. Improvise and adapt to define your modus operandi.",
    ratings: [
      {
        _id: ObjectId(),
        author: "myUserId1",
        pointsAction: NumberInt(5),
        pointsAddiction: NumberInt(5),
        pointsGraphics: NumberInt(3),
        pointsSound: NumberInt(4),
        comment: "My all time favorite game.",
      },
    ],
  },
  {
    title: "FIFA 21",
    publisher: "Electronic Arts",
    developer: "Electronic Arts, EA Vancouver",
    releaseDate: new Date("2020-10-06"),
    platforms: [
      "PC",
      "PlayStation 4",
      "PlayStation 5",
      "Stadia",
      "Switch",
      "Xbox One",
      "Xbox Series X",
    ],
    coverUrl:
      "https://images.igdb.com/igdb/image/upload/t_cover_big/co2e0n.jpg",
    description:
      "On the street and in the stadium, FIFA 21 has more ways to play than ever before. FIFA 21 rewards you for your creativity and control all over the pitch. Create more scoring opportunities with all-new dynamic attacking systems in the most intelligent FIFA gameplay to date. A new Agile Dribbling system gives you the means to unleash your creativity in 1-on-1 situations. Use fast footwork, more responsive close control, and new skill moves like the ball roll fake to explode past defenders. In FIFA 21, increased positional awareness elevates footballers’ in-game intelligence to put them in the right place at the right time. See world-class forwards hold their runs in line with the last defender, creative playmakers find space to play through balls, and midfielders shut off passing lanes as players better live up to their real-world understanding of space and time on the pitch.",
    trailerUrl: "https://www.youtube.com/watch?v=tuLAn9adQpI",
    ratings: [
      {
        _id: ObjectId(),
        author: "myUserId1",
        pointsAction: NumberInt(1),
        pointsAddiction: NumberInt(3),
        pointsGraphics: NumberInt(4),
        pointsSound: NumberInt(3),
        comment: "it's okay... juve is missing!",
      },
      {
        _id: ObjectId(),
        author: "myUserId2",
        pointsAction: NumberInt(1),
        pointsAddiction: NumberInt(1),
        pointsGraphics: NumberInt(2),
        pointsSound: NumberInt(1),
        comment: "FUT PAY2WIN WTF?!",
      },
      {
        _id: ObjectId(),
        author: "myUserId3",
        pointsAction: NumberInt(3),
        pointsAddiction: NumberInt(5),
        pointsGraphics: NumberInt(4),
        pointsSound: NumberInt(4),
        comment: "no career mode updates... lame!",
      },
    ],
  },
  {
    title: "The Last of Us",
    publisher: "SCEA",
    developer: "Naughty Dog",
    releaseDate: new Date("2013-06-14"),
    platforms: ["PlayStation 3"],
    coverUrl:
      "https://images.igdb.com/igdb/image/upload/t_cover_big/co1r7f.jpg",
    description:
      " Twenty years after a pandemic radically transformed known civilization, infected humans run amuck and survivors kill one another for sustenance and weapons - literally whatever they can get their hands on. Joel, a salty survivor, is hired to smuggle a fourteen-year-old girl, Ellie, out of a rough military quarantine, but what begins as a simple job quickly turns into a brutal journey across the country.",
    trailerUrl: "https://www.youtube.com/watch?v=Co0mF_imNys",
    ratings: [],
  },
  {
    title: "Immortals Fenyx Rising",
    publisher: "Ubisoft",
    developer: "Ubisoft Quebec",
    releaseDate: new Date("2020-12-03"),
    platforms: [
      "PC",
      "PlayStation 4",
      "PlayStation 5",
      "Stadia",
      "Switch",
      "Xbox One",
      "Xbox Series X",
    ],
    coverUrl:
      "https://images.igdb.com/igdb/image/upload/t_cover_big/co2g0b.jpg",
    description:
      "Immortals Fenyx Rising brings grand mythological adventure to life. Play as Fenyx, a new, winged demigod, on a quest to save the Greek gods. The fate of the world is at stake – you are the gods’ last hope. Wield the powers of the gods like Achilles' sword and Daidalos' wings to battle powerful enemies and solve ancient puzzles. Fight iconic mythological beasts like Cyclops and Medusa in dynamic combat in the air and on the ground. Use your skills and diverse weapons, including selfguided arrows, telekinesis, and more, for devastating damage. Discover a stylized open world across seven unique regions, each inspired by the gods.",
    trailerUrl: "https://www.youtube.com/watch?v=U4zb1yiFlQo",
    ratings: [],
  },
  {
    title: "Assassin's Creed Valhalla",
    publisher: "Ubisoft",
    developer: "Ubisoft Montreal",
    releaseDate: new Date("2020-11-10"),
    platforms: [
      "PC",
      "PlayStation 4",
      "PlayStation 5",
      "Stadia",
      "Xbox One",
      "Xbox Series X",
    ],
    coverUrl:
      "https://images.igdb.com/igdb/image/upload/t_cover_big/co2ed3.jpg",
    description:
      " Build your own Viking Legend. Become Eivor, a Viking raider raised to be a fearless warrior, and lead your clan from icy desolation in Norway to a new home amid the lush farmlands of ninth-century England. Find your settlement and conquer this hostile land by any means to earn a place in Valhalla. England in the age of the Vikings is a fractured nation of petty lords and warring kingdoms. Beneath the chaos lies a rich and untamed land waiting for a new conqueror. Will it be you? Write Your Viking Saga. Blaze your own path across England with advanced RPG mechanics. Fight brutal battles, lead fiery raids or use strategy and alliances with other leaders to bring victory. Every choice you make in combat and conversation is another step on the path to greatness. Lead Epic Raids. Lead a crew of raiders and launch lightning-fast surprise attacks against Saxon armies and fortresses. Claim the riches of your enemies' lands for your clan and expand your influence far beyond your growing settlement. ENGAGE IN VISCERAL COMBAT. Unleash the ruthless fighting style of a Viking warrior as you dual-wield axes, swords, or even shields against relentless foes. Decapitate opponents in close-quarters combat, riddle them with arrows, or assassinate them with your Hidden Blade. Grow Your Settlement. Your clan's new home grows with your legend. Customise your settlement by building upgradable structures. Unlock new features and quests by constructing a barracks, a blacksmith, a tattoo parlour, and much more. Share Your Custom Raider. Recruit mercenary Vikings designed by other players or create and customise your own to share online. Sit back and reap the rewards when they fight alongside your friends in their game worlds. A Dark Age Open World. Sail across the icy North Sea to discover and conquer the broken kingdoms of England. Immerse yourself in activities like hunting and drinking games or engage in traditional Norse competitions like flyting – or, as it's better known, verbally devastating rivals through the art of the Viking rap battle.",
    trailerUrl: "https://www.youtube.com/watch?v=ssrNcwxALS4",
    ratings: [],
  },
  {
    title: "Hades",
    publisher: "Private Division",
    developer: "Supergiant Games",
    releaseDate: new Date("2021-09-17"),
    platforms: [
      "PC",
      "PlayStation 4",
      "PlayStation 5",
      "Xbox One",
      "Xbox Series X",
    ],
    coverUrl:
      "https://images.igdb.com/igdb/image/upload/t_cover_big/co39vc.jpg",
    ratings: [],
  },
  {
    title: "Ori ant the Will of the Wisps",
    publisher: "Xbox Game Studios",
    developer: "Moon Studios",
    releaseDate: new Date("2020-11-10"),
    platforms: ["PC", "Xbox One", "Xbox Series X", "Switch"],
    coverUrl:
      "https://images.igdb.com/igdb/image/upload/t_cover_big/co2e1l.jpg",
    description:
      " The little spirit Ori is no stranger to peril, but when a fateful flight puts the owlet Ku in harm’s way, it will take more than bravery to bring a family back together, heal a broken land, and discover Ori’s true destiny. From the creators of the acclaimed action-platformer Ori and the Blind Forest comes the highly anticipated sequel. Embark on an all-new adventure in a vast world filled with new friends and foes that come to life in stunning, hand-painted artwork. Set to a fully orchestrated original score, Ori and the Will of the Wisps continues the Moon Studios tradition of tightly crafted platforming action and deeply emotional storytelling. ",
    trailerUrl: "https://www.youtube.com/watch?v=2kPSl2vyu2Y",
    ratings: [],
  },
  {
    title: "Factorio",
    publisher: "Wube Software LTD.",
    developer: "Wube Software LTD.",
    releaseDate: new Date("2020-08-14"),
    platforms: ["PC"],
    coverUrl:
      "https://images.igdb.com/igdb/image/upload/t_cover_big/co1tfy.jpg",
    description:
      " Factorio is a game about building and creating automated factories to produce items of increasing complexity, within an infinite 2D world. Use your imagination to design your factory, combine simple elements into ingenious structures, and finally protect it from the creatures who don't really like you. ",
    trailerUrl: "https://www.youtube.com/watch?v=DR01YdFtWFI",
    ratings: [],
  },
  {
    title: "Super Mario 3D World",
    publisher: "Nintendo",
    developer: "Nintendo",
    releaseDate: new Date("2013-11-22"),

    platforms: ["Wii U"],
    coverUrl:
      "https://images.igdb.com/igdb/image/upload/t_cover_big/co21vd.jpg",
    description:
      " Work together with your friends or compete for the crown in the first multiplayer 3D Mario game for the Wii U console. In the Super Mario 3D World game, players can choose to play as Mario, Luigi, Princess Peach or Toad.",
    trailerUrl: "https://www.youtube.com/watch?v=wLOKVABfrzw",
    ratings: [],
  },
  {
    title: "Super Mario World",
    publisher: "Nintendo",
    developer: "Nintendo",
    releaseDate: new Date("1992-04-11"),
    platforms: ["SNES"],
    coverUrl:
      "https://images.igdb.com/igdb/image/upload/t_cover_big/co2v5y.jpg",
    description:
      "During a vacation in Dinosaur Land, Princess Toadstool gets kidnapped, and a spell is cast on the inhabitants of the island. When they stumble upon Yoshi, a young dinosaur, Mario and Luigi learn that Bowser is responsible for the terrible misdeeds. Now, all Yoshis are trapped in magical eggs that Bowser has hidden throughout seven castles. Many hidden paths aid Mario in making his way to Bowser's castle, completing 74 areas, and finding all 96 exits. Discover items, including a feather that gives Mario a cape that allows him to fly or a flower that shoots fireballs, in layers upon layers of 2-D scrolling landscapes. You can even ride Yoshi and swallow your enemies!",
    trailerUrl: "https://www.youtube.com/watch?v=RJ1w-venSAE",
    ratings: [],
  },
  {
    title: "OMNO",
    publisher: "Future Friends Games",
    developer: "StudioInkyfox",
    releaseDate: new Date("2021-07-29"),
    platforms: ["PC", "PS4", "Xbox One"],
    coverUrl:
      "https://images.igdb.com/igdb/image/upload/t_cover_big/co3dli.jpg",
    description:
      "A single-player journey of discovery through an ancient world of wonders. Full of puzzles, secrets and obstacles to overcome, where the power of a lost civilization will carry you through forests, deserts and tundras - even to the clouds. Besides offering an interactive world, Omno challenges you with puzzles, hidden secrets and obstacles to overcome in 3D puzzle platformer style.",
    trailerUrl: "https://www.youtube.com/watch?v=ISDUQhv47-E&t=5s",
    ratings: [],
  },
  {
    title: "F1 2021",
    publisher: "EA",
    developer: "Codemasters",
    releaseDate: new Date("2021-07-16"),
    platforms: ["PC", "PS4", "Xbox One", "PS5", "Xbox Series X"],
    coverUrl:
      "https://images.igdb.com/igdb/image/upload/t_cover_big/co31gd.jpg",
    description:
      "Every story has a beginning in F1 2021, the official video game of the 2021 FIA FORMULA ONE WORLD CHAMPIONSHIP. Enjoy the stunning new features of F1 2021, including the thrilling story experience ‘Braking Point’, two-player Career, and get even closer to the grid with ‘Real-Season Start’.",
    trailerUrl: "https://www.youtube.com/watch?v=S985xPL9Xn0",
    ratings: [],
  },
  {
    title: "Secret of Mana",
    publisher: "Nintendp",
    developer: "Square",
    releaseDate: new Date("1993-08-06"),
    platforms: ["SNES"],
    coverUrl:
      "https://images.igdb.com/igdb/image/upload/t_cover_big/co2u6w.jpg",
    description:
      "Initially released in Japan in 1993, Secret of Mana took the world by storm with its innovative real-time battle system and gorgeously rendered world. It continues to stand out among other action RPGs for its seamless gameplay that anyone from beginner to veteran can enjoy.\nOne of the most memorable elements of the Mana series is the Ring Command menu system. With the single press of a button, a ring-shaped menu appears on the screen, where players can use items, change weapons, and do a variety of other actions without needing to switch screens. This Ring Command menu system for which the Mana series is so well known was first introduced in Secret of Mana and has since appeared in most games in the series.\nPlay as Randi and his two companions, Primm and Popoi, as they adventure all around the world. At the center of our epic story is the mystical power of Mana. Battle the empire in its quest for control of Mana. Befriend the eight elementals who wield the forces of nature itself. Numerous encounters await at every turn.",
    trailerUrl: "https://www.youtube.com/watch?v=FwGd2kQ9OsM",
    ratings: [],
  },
  {
    title: "Metal Gear Solid",
    publisher: "Konami",
    developer: "KCEJ",
    releaseDate: new Date(""),
    platforms: ["PC", "PS4", "Xbox One", "PS5", "Xbox Series X"],
    coverUrl:
      "https://images.igdb.com/igdb/image/upload/t_cover_big/co1v82.jpg",
    description:
      "A third-person story-heavy stealth action title which follows Solid Snake, a special operations soldier, who infiltrates a nuclear weapons facility by the name of Shadow Moses Island to neutralize a renegade special forces unit named FOXHOUND. Throughout his journey, he uncovers various lies and military conspiracies, and his ideas of trust, violence and control are challenged.",
    trailerUrl: "https://www.youtube.com/watch?v=5ecSzNoDxuM",
    ratings: [],
  },
  {
    title: "TETRIS",
    publisher: "Nintendo",
    developer: "Nintendo R&D1",
    releaseDate: new Date("1990-09-28"),
    platforms: ["Game Boy"],
    coverUrl:
      "https://images.igdb.com/igdb/image/upload/t_cover_big/co2ufk.jpg",
    description:
      "Tetris is a tile-matching puzzle video game. The goal is to place pieces made up of four tiles in a ten-by-twenty well, organizing them into complete rows, which then disappear. The main objective of each round is to clear 25 lines, after which the player moves on to the next round. If the stack reaches the top of the field, the player loses a life, and if all three lives are lost, the game is over.\n\nThe game lets the player choose the starting stage and round, as well as one of three background tunes. Difficulty is increased throughout the stages by an increase in speed and the addition of garbage blocks in the well.",
    trailerUrl: "https://www.youtube.com/watch?v=uRrTA8NqPpQ",
    ratings: [],
  },
]);
