# Übungsprojekt von Dominik Alexander

## Wahl der Technologien

| Teilanwendung | Bereich                   | gewählte Technologie    |
| ------------- | ------------------------- | ----------------------- |
| Backend       | Schnittstellentechnologie | REST API                |
|               | Framework                 | Spring (Boot)           |
|               | Programmiersprache        | Kotlin                  |
|               | IDE                       | IntelliJ IDEA CE        |
|               | Test Frameworks           | JUnit 5                 |
|               |                           | MockK                   |
| Frontend      | Technologie               | Single-page application |
|               | Library                   | React                   |
|               | Programmiersprache        | TypeScript              |
|               | CSS Library               | Bulma                   |
|               | IDE / Editor              | VS Code                 |
|               | Test Frameworks           | jest                    |
|               |                           | react-testing-library   |
| Datenbank     | Technologie               | NoSQL Datenbank Server  |
|               | Runtime                   | Docker Container        |
|               | Datenbank                 | MongoDB                 |
|               | Management Tool           | Robo3T                  |

---

## Projekt zum Laufen bringen

### 1. MongoDB Server starten

Als erstes muss der Docker Container mit der MongoDB Instanz gestartet werden, dessen Konfigurationsdateien sich in `/database` befinden. Dort findet sich auch eine ausführlichere [README.md](./database/README.md) für weitere Informationen. Wenn Docker Desktop installiert ist, lässt sich der Container mit dem Befehl

```
cd database
docker-compose [-f docker-compose.yml] up
```

starten. (-f docker-compose.yml ist optional, da dies der default Dateiname ist.)

### 2. Backend starten

Sobald der Datenbank Server läuft, kann das Backend gestartet werden. Dazu das Projekt, welches sich in `/backend` befindet in bspw. IntelliJ IDEA öffnen und F5 drücken.

### 3. Keycloak starten

Eine Keycloak Instanz kann per

```
cd keycloak
docker-compose up
```

gestartet werden.

Das Admin-Panel kann danach unter http://localhost:3003/auth/admin/ aufgerufen werden.

| Username | Passwort |
| -------- | -------- |
| admin    | Pa55w0rd |

Diese Zugangsdaten können in der docker-compose.yml im keycloak Ordner angepasst werden.

### 4. Frontend starten

Das Frontend kann per

```
cd frontend
npm start
```

gestartet werden.

---

### Ports

| Komponente | Port                          |
| ---------- | ----------------------------- |
| frontend   | [3000](http://localhost:3000) |
| backend    | [3001](http://localhost:3001) |
| database   | [3002](http://localhost:3002) |
| keycloak   | [3003](http://localhost:3003) |

## Links

- [Jira Board](https://lise.atlassian.net/jira/software/projects/UPDA/boards/151)

(c) Dominik Alexander
